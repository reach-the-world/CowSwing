/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.core.event;

import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;


/**
 * 事件观察者
 *@author DuanYong
 *@since 2012-11-4下午10:34:22
 *@version 1.0
 */
public class CowSwingObserver {
	private Logger logger = Logger.getLogger(this.getClass());
	private static CowSwingObserver observer;
	private CopyOnWriteArrayList<CowSwingListener> repository = new CopyOnWriteArrayList<CowSwingListener>();
	
	private CowSwingObserver(){}
	
	public static CowSwingObserver getInstance(){
		if( observer== null){
			observer = new CowSwingObserver();
		}
		return observer;
	}
	public void addCrawlerListener(CowSwingListener crawlerListener) {
		repository.add(crawlerListener);
	}

	public void removeCrawlerListener(CowSwingListener crawlerListener) {
		repository.remove(crawlerListener);
	}

	@SuppressWarnings("unchecked")
	public void notifyEvents(CowSwingEvent crawlerEvent) {
		CopyOnWriteArrayList<CowSwingListener> tempList;
		synchronized (this) {
			tempList = (CopyOnWriteArrayList<CowSwingListener>) repository.clone();
			if(CollectionUtils.isNotEmpty(tempList)){
                tempList.forEach(listener->listener.update(crawlerEvent));
			}
		}

	}
}
