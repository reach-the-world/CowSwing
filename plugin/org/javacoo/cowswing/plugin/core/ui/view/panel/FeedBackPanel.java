/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.core.ui.view.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.ui.listener.TextVerifier;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;

import com.javacoo.webservice.manager.beans.FeedBackBean;
import org.springframework.stereotype.Component;

/**
 * 意见反馈信息PANEL
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2014-9-30 上午11:27:07
 * @version 1.0
 */
@Component("feedBackPanel")
public class FeedBackPanel extends AbstractContentPanel<FeedBackBean>{
	private static final long serialVersionUID = 1L;
	protected Logger logger = Logger.getLogger(this.getClass());
	/**电子邮件标签*/
	private javax.swing.JLabel emailLabel;
	/**电子邮件Field*/
	private javax.swing.JTextField emailField;
	/**电话标签*/
	private javax.swing.JLabel phoneLabel;
	/**电话Field*/
	private javax.swing.JTextField phoneField;
	/**qq标签*/
	private javax.swing.JLabel qqLabel;
	/**qqField*/
	private javax.swing.JTextField qqField;
	/**标题标签*/
	private javax.swing.JLabel titleLabel;
	/**标题Field*/
	private javax.swing.JTextField titleField;
	/**内容标签*/
	private javax.swing.JLabel contentLabel;
	/**内容JTextArea*/
	private javax.swing.JTextArea contentArea;
	/* (non-Javadoc)
	 * @see org.javacoo.cowswing.ui.view.panel.AbstractContentPanel#populateData()
	 */
	@Override
	protected FeedBackBean populateData() {
		FeedBackBean feedBackBean = new FeedBackBean();
		feedBackBean.setContent(contentArea.getText());
		feedBackBean.setEmail(emailField.getText());
		feedBackBean.setPhone(phoneField.getText());
		feedBackBean.setQq(qqField.getText());
		feedBackBean.setTitle(titleField.getText());
		return feedBackBean;
	}
	/* (non-Javadoc)
	 * @see org.javacoo.cowswing.ui.view.panel.AbstractContentPanel#initComponents()
	 */
	@Override
	protected void initComponents() {
		useGridBagLayout();
		emailLabel = new javax.swing.JLabel();
		emailLabel.setText(LanguageLoader.getString("Core.feedBack.email"));
		addCmpToGridBag(emailLabel,new GBC(0,0));
		
		
		emailField = new javax.swing.JTextField();
		addCmpToGridBag(emailField,new GBC(1,0));
		
		
		phoneLabel = new javax.swing.JLabel();
		phoneLabel.setText(LanguageLoader.getString("Core.feedBack.phone"));
		addCmpToGridBag(phoneLabel,new GBC(0,1));
		
		phoneField = new javax.swing.JTextField();
		addCmpToGridBag(phoneField,new GBC(1,1));
		
		
		qqLabel = new javax.swing.JLabel();
		qqLabel.setText(LanguageLoader.getString("Core.feedBack.qq"));
		addCmpToGridBag(qqLabel,new GBC(0,2));
		
		qqField = new javax.swing.JTextField();
		addCmpToGridBag(qqField,new GBC(1,2));
		
		titleLabel = new javax.swing.JLabel();
		titleLabel.setText(LanguageLoader.getString("Core.feedBack.title"));
		addCmpToGridBag(titleLabel,new GBC(0,3));
		
		titleField = new javax.swing.JTextField();
		titleField.setInputVerifier(new TextVerifier(this,false));
		addCmpToGridBag(titleField,new GBC(1,3));
		
		
		contentLabel = new javax.swing.JLabel();
		contentLabel.setText(LanguageLoader.getString("Core.feedBack.content"));
		addCmpToGridBag(contentLabel,new GBC(0,4));
		
		contentArea = new javax.swing.JTextArea(4,40);
		JPanel replaceWordAreaPanel = new JPanel(new BorderLayout()); 
		contentArea.setLineWrap(true);
		contentArea.setWrapStyleWord(true);//激活断行不断字功能 
		replaceWordAreaPanel.add(new JScrollPane(contentArea));
		addCmpToGridBag(replaceWordAreaPanel,new GBC(1,4));
		
		addCmpToGridBag(new JLabel(),new GBC(0,5,2,1));
	
	}
	protected void initActionListener(){
		
	}
	public void initData(FeedBackBean t){
		fillComponentData(t);
	}
	/* (non-Javadoc)
	 * @see org.javacoo.cowswing.ui.view.panel.AbstractContentPanel#fillComponentData(java.lang.Object)
	 */
	@Override
	protected void fillComponentData(FeedBackBean t) {
		
		
	}
	
}
