package org.javacoo.cowswing.plugin.gather.service.export.manager;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.ui.freemarker.SpringTemplateLoader;

import freemarker.template.Configuration;
import freemarker.template.Template;


/**
 * 下载文件模板管理
 * @author DuanYong
 * @since 2012-07-20
 */
public class FreemarkerTemplateManager implements TemplateManager<Template> { 
	private static Log logger = LogFactory.getLog(FreemarkerTemplateManager.class);
	
	private static Map<String, Template> templateConfigs = null;
	
	private final static String DEFAULT_TEMPLATEFILE = "templates/single/*.ftl";
	private final static String MULTIPLE_TEMPLATEFILE = "templates/multiple/*.ftl";
	private String strSingleTemplateFile;
	private String strMultipleTemplateFile;

	/**
	 * 根据配置文件初始化 templateConfigs
	 */
    public FreemarkerTemplateManager(String strSingleTemplateFile,String strMultipleTemplateFile){
    	this.strSingleTemplateFile = StringUtils.isBlank(strSingleTemplateFile)?DEFAULT_TEMPLATEFILE:strSingleTemplateFile;
    	this.strMultipleTemplateFile = StringUtils.isBlank(strSingleTemplateFile)?MULTIPLE_TEMPLATEFILE:strMultipleTemplateFile;
    	templateConfigs = new HashMap<String, Template>();
    	genRealTemplates(this.strSingleTemplateFile,GatherConstant.SINGLE_TEMPLATE_NAME_LIST);
    	genRealTemplates(this.strMultipleTemplateFile,GatherConstant.MULTIPLE_TEMPLATE_NAME_LIST);
		logger.info("初始化加载Templates完成");
    }
	/**
	 * 根据模板ID取得模板
	 * <p>方法说明:</p>
	 * @since 2012-7-26 下午4:15:12
	 * @param templateId 模板ID
	 * @return T
	 */
	public Template findTemplate(String templateId){
		return templateConfigs.get(templateId);
	}
	/**
	 * 加载模板
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月27日上午9:33:40
	 * @param templatePath
	 * @param templateList
	 */
	private void genRealTemplates(String templatePath,List<String> templateList) {
		String[] aryConfigFile = new String[]{templatePath};
		String templateDir = null;
		String strFileName = null;
		String strFileId = null;
		for(String strResource:aryConfigFile)
		{
			ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
			try {
				Resource[] resources = resourcePatternResolver.getResources(strResource);
				templateDir = templatePath.substring(0,templatePath.lastIndexOf('/'));
				Configuration freemarker_cfg = getFreeMarkerCfg(templateDir);
				for(int i =0;i<resources.length;i++){
					strFileName = resources[i].getURL().toString();
					strFileId = strFileName.substring(strFileName.lastIndexOf('/')+1,strFileName.lastIndexOf('.'));
					strFileName = strFileName.substring(strFileName.lastIndexOf('/')+1);
					//tmpList.add(resources[i].getURL());
					templateConfigs.put(strFileId,freemarker_cfg.getTemplate(strFileName));
					templateList.add(strFileId);
					logger.info(String.format("加载模板配置%s成功",strFileName));
				}
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("生成html文本发生IO错误");
			}
			catch(Exception e){
				logger.error(String.format("根据配置%s获取配置文件名出错",templateDir+strFileName),e);
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 获取freemarker的配置. freemarker本身支持classpath,目录和从ServletContext获取.
	 */
	protected Configuration getFreeMarkerCfg(String strDir) {
		ResourceLoader resourceLoader = new DefaultResourceLoader();
		Configuration freemarker_cfg = null;
		try {
			freemarker_cfg = new Configuration();
			//freemarker_cfg.setClassForTemplateLoading(this.getClass(),"");
			freemarker_cfg.setEncoding(Locale.getDefault(), "UTF-8");
			//FileTemplateLoader fileTemplateLoader = new FileTemplateLoader();
			SpringTemplateLoader springTemplateLoader = new SpringTemplateLoader(resourceLoader,strDir);
			freemarker_cfg.setTemplateLoader(springTemplateLoader);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("初始化TemplateLoader出错");
		}

		return freemarker_cfg;
	}

	
}