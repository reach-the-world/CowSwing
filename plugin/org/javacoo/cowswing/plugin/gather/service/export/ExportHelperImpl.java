package org.javacoo.cowswing.plugin.gather.service.export;

import java.util.Map;

import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportContentBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportParamBean;
import org.javacoo.cowswing.plugin.gather.service.export.builder.ExportBeanBuilder;
import org.javacoo.cowswing.plugin.gather.service.export.factory.AbstractExportFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 文件下载实现类
 * <p>
 * 
 * @author DuanYong
 * @since 2012-07-20
 * @version 1.0
 * @updated 2012-07-20
 */
@Service("exportHelper")
public class ExportHelperImpl extends AbstractExportFactory implements ExportHelper {
	/**构建下载内容bena服务MAP*/
	@Autowired
	private Map<String,ExportBeanBuilder> exportBeanBuilderMap;
	private ExportParamBean exportParamBean = new ExportParamBean();
	
	/**
	 * 生成文件下载bean
	 * @param fileType 文件类别
	 * @param fileName 文件名称
	 * @param templateName 模板名称
	 * @param resultMap 结果集
	 * @return 文件下载bean
	 */
	public ExportBean generateExportBean(String fileType, String fileName, String templateName,
			Map<Object,Object> resultMap) {
		init(fileType,fileName,templateName,resultMap);
		return this.createDownloadFile();
	}
    /**
     * 得到文件内容bean
     */
	@Override
	protected ExportContentBean getExportContentBean() {
		return lookupHandlerBy(exportParamBean.getFileType()).build(exportParamBean);
	}
	/**
	 * 初始化查询参数bean
	 * @param fileType 生成的文件类型
	 * @param fileName 生成的文件名
	 * @param templateName 模板名称
	 * @param resultMap 数据map
	 */
	private void init(String fileType, String fileName, String templateName,
			Map<Object,Object> resultMap){
		exportParamBean.setFileName(fileName);
		exportParamBean.setFileType(fileType);
		exportParamBean.setResultMap(resultMap);
		exportParamBean.setTemplateName(templateName);
	}
	/**
	 * 根据类型名称查找已经注册的构建下载文件内容bean服务对象
	 * @param handlerName 类型名称
	 * @return 生成文件内容服务
	 */
	private ExportBeanBuilder lookupHandlerBy(String handlerName){
		if(null != exportBeanBuilderMap.get(handlerName)){
			return exportBeanBuilderMap.get(handlerName);
		}else{
			return null;
			//throw new BusinessException("不支持生成此类型的文件");
		}
		
	}
}
