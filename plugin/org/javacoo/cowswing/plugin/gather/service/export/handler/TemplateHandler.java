package org.javacoo.cowswing.plugin.gather.service.export.handler;

import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportContentBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportParamBean;

/**
 * 模板解析接口
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18上午10:49:29
 * @version 1.0
 */
public interface TemplateHandler {
	/**
	 * 组装模板
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-2-18上午10:48:58
	 * @version 1.0
	 * @param exportContentBean 导出数据内容值对象
	 * @param exportParamBean 数据导出参数配置对象
	 */
	void populateTemplate(ExportContentBean exportContentBean,ExportParamBean exportParamBean);
}
