package org.javacoo.cowswing.plugin.gather.service.export.factory;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportBean;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportContentBean;
import org.javacoo.crawler.core.util.CommonUtils;

/**
 * 文件下载抽象类
 * <p>
 * 定义了一些公用方法和主要流程
 * @author DuanYong
 * @since 2016-2-18下午3:57:03
 * @version 1.0
 */
public abstract class AbstractExportFactory implements ExportFactory {
	protected Log logger = LogFactory.getLog(AbstractExportFactory.class);
	protected ExportContentBean exportContentBean;
	/**
     * 生成下载文件
     */
	public ExportBean createDownloadFile(){
		exportContentBean = getExportContentBean();
		ExportBean fileDownloadBean = new ExportBean();
		fileDownloadBean.setContentType(exportContentBean.getDownloadType());
		fileDownloadBean.setFileName(CommonUtils.stringFilter(exportContentBean.getFileName()));
//		try {
//			fileDownloadBean.setFileName(URLEncoder.encode(exportContentBean.getFileName(),"UTF-8"));
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
 	    fileDownloadBean.setContent(exportContentBean.getStringContent());
 	    fileDownloadBean.setByteContent(exportContentBean.getByteContent());
 	    return fileDownloadBean;
	}
	/**
     * 得到文件内容bean
     */
	protected abstract ExportContentBean getExportContentBean();
	
}
