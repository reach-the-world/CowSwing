package org.javacoo.cowswing.plugin.gather.service.export.builder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.javacoo.cowswing.core.utils.FileUtils;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.springframework.stereotype.Component;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;

/**
 * 生成PDF
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午2:45:39
 * @version 1.0
 */
@Component(GatherConstant.EXPORT_TYPE_PDF)
public class ExportPdfBuilder extends AbstractExportBeanBuilder{
	private final static String FONT_FILE_SIMSUN = "resources/fonts/simsun.ttc";
	private final static String PDF_FILE_EXTENSION = ".pdf";
    private final static String PDF_CONTENT_TYPE = "application/pdf";
	@Override
	protected void buildExportContentBean() {
		this.exportContentBean.setDownloadType(PDF_CONTENT_TYPE);
		this.exportContentBean.setFileName(this.exportParamBean.getFileName() + PDF_FILE_EXTENSION);
		this.exportContentBean.setByteContent(getByteContent());
	}
	/**
     * 取得组装好的PDF文件 byte[]
     * @return
     */
	private byte[] getByteContent(){
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
	    	ITextRenderer renderer = new ITextRenderer();
			ITextFontResolver resolver = renderer.getFontResolver();
			resolver.addFont(FileUtils.findAsResource(FONT_FILE_SIMSUN).getFile(), BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
			renderer.setDocumentFromString(this.exportContentBean.getStringContent());
			renderer.layout();
			renderer.createPDF(out);
		}
		catch (DocumentException e) {
			e.printStackTrace();
			logger.error("生成PDF失败");
		}
		catch (IOException e) {
			e.printStackTrace();
			logger.error("生成PDF失败");
		}
	    return out.toByteArray();
	}
    
	

}
