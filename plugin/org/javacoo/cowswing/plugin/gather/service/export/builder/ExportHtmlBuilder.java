package org.javacoo.cowswing.plugin.gather.service.export.builder;


import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.springframework.stereotype.Component;

/**
 * 生成Html
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午3:26:52
 * @version 1.0
 */
@Component(GatherConstant.EXPORT_TYPE_HTML)
public class ExportHtmlBuilder extends AbstractExportBeanBuilder{
	private final static String HTML_FILE_EXTENSION = ".html";
    private final static String HTML_CONTENT_TYPE = "text/html";
	@Override
	protected void buildExportContentBean() {
		this.exportContentBean.setDownloadType(HTML_CONTENT_TYPE);
		this.exportContentBean.setFileName(this.exportParamBean.getFileName() + HTML_FILE_EXTENSION);
		this.exportContentBean.setByteContent(getByteContent());
	}
    /**
     * 取得组装好的excel文件 byte[]
     * @return
     */
	private byte[] getByteContent(){
		return this.exportContentBean.getByteContent();
	}
    
	

}
