/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.javacoo.cowswing.base.service.ICrawlerService;
import org.javacoo.cowswing.core.cache.ICowSwingCacheManager;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.utils.DateFormatUtils;
import org.javacoo.cowswing.core.utils.DateUtil;
import org.javacoo.cowswing.core.utils.JsonUtils;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.javacoo.cowswing.plugin.gather.service.beans.ColumnBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentResourceBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentResourceCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerExtendFieldCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.SelectValueBean;
import org.javacoo.crawler.core.data.ContentBean;
import org.javacoo.crawler.core.data.CrawlerExtendFieldBean;
import org.javacoo.crawler.core.data.uri.CrawlResURI;
import org.javacoo.persistence.util.DBConnectionManager;
import org.springframework.beans.BeanUtils;

/**
 * 抽象远程数据库公共服务类
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-1-26上午9:19:22
 * @version 1.0
 */
public abstract class AbstractSaveRemoteContent extends AbstractThreadService{
	protected Logger log = Logger.getLogger(this.getClass());
	/**规则服务类*/
	protected ICrawlerService<CrawlerRuleBean,CrawlerRuleCriteria> crawlerRuleService;
	/**采集内容服务类*/
	protected ICrawlerService<CrawlerContentBean,CrawlerContentCriteria> crawlerContentService;
	/**采集内容评论服务类*/
	protected ICrawlerService<CrawlerContentCommentBean,CrawlerContentCommentCriteria> crawlerContentCommentService;
	/**采集内容分页服务类*/
	protected ICrawlerService<CrawlerContentPaginationBean,CrawlerContentPaginationCriteria> crawlerContentPaginationService;
	/**扩展字段服务类*/
	protected ICrawlerService<CrawlerExtendFieldBean,CrawlerExtendFieldCriteria> crawlerExtendFieldService;
	/**扩展字段服务类*/
	protected ICrawlerService<CrawlerContentResourceBean,CrawlerContentResourceCriteria> crawlerContentResourceService;
	/**数据库连接管理*/
	protected DBConnectionManager connectionManager;
	/**缓存管理*/
	protected ICowSwingCacheManager crawlerCacheManager;
	/**规则ID*/
	protected Integer ruleId;
	/**随机静态值MAP*/
	protected Map<String,SelectValueBean> randomValueMap = new HashMap<String,SelectValueBean>();
	/**随机静态值*/
	protected Map<String,String> randomValue = new HashMap<String,String>();
	
	
	/**
	 * @param crawlerRuleService
	 * @param crawlerContentService
	 * @param crawlerContentCommentService
	 * @param crawlerContentPaginationService
	 * @param crawlerExtendFieldService
	 * @param connectionManager
	 * @param ruleId
	 */
	public AbstractSaveRemoteContent(
			ICrawlerService<CrawlerRuleBean, CrawlerRuleCriteria> crawlerRuleService,
			ICrawlerService<CrawlerContentBean, CrawlerContentCriteria> crawlerContentService,
			ICrawlerService<CrawlerContentCommentBean, CrawlerContentCommentCriteria> crawlerContentCommentService,
			ICrawlerService<CrawlerContentPaginationBean, CrawlerContentPaginationCriteria> crawlerContentPaginationService,
			ICrawlerService<CrawlerExtendFieldBean, CrawlerExtendFieldCriteria> crawlerExtendFieldService,
			ICrawlerService<CrawlerTaskBean,CrawlerTaskCriteria> crawlerTaskService,
			ICrawlerService<CrawlerContentResourceBean,CrawlerContentResourceCriteria> crawlerContentResourceService,
			DBConnectionManager connectionManager, 
			ICowSwingCacheManager crawlerCacheManager, 
			Integer ruleId) {
		super(crawlerTaskService);
		this.crawlerRuleService = crawlerRuleService;
		this.crawlerContentService = crawlerContentService;
		this.crawlerContentCommentService = crawlerContentCommentService;
		this.crawlerContentPaginationService = crawlerContentPaginationService;
		this.crawlerExtendFieldService = crawlerExtendFieldService;
		this.crawlerContentResourceService = crawlerContentResourceService;
		this.connectionManager = connectionManager;
		this.crawlerCacheManager = crawlerCacheManager;
		this.ruleId = ruleId;
	}
	

	/**
	 * 修改内容状态为已保存
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2013-5-1 下午5:18:44
	 * @version 1.0
	 * @exception 
	 * @param crawlerContentBean 内容值对象
	 */
	protected void updateContentState(CrawlerContentBean crawlerContentBean) {
		CrawlerContentBean contentBean = new CrawlerContentBean();
		contentBean.setContentId(crawlerContentBean.getContentId());
		contentBean.setHasSave(Constant.YES);
		crawlerContentService.update(contentBean, GatherConstant.SQLMAP_ID_UPDATE_CRAWLER_CONTENT);
	}

	/**
	 * 插入内容至指定数据库
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午9:53:39
	 * @version 1.0
	 * @param contentBean 内容值对象
	 * @param rule 采集规则对象
	 */
	protected void insertContentToTargetDataBase(ContentBean contentBean,CrawlerRuleBean rule) {
		Map<String,Map<String,ColumnBean>> tempMap = rule.getRuleDataBaseBean().getHasSelectedValueMap();
		Map<String,Map<String,ColumnBean>> tableMap = new HashMap<String,Map<String,ColumnBean>>();
		if(!tempMap.isEmpty()){
			String key = "";
			for(Iterator<String> it = tempMap.keySet().iterator();it.hasNext();){
				key = it.next();
				tableMap.put(key, tempMap.get(key));
			}
		}
		//主表
		String primaryTableName = rule.getRuleDataBaseBean().getPrimaryTable();
		//内容表
		String contentTableName = findTableNameByType(tableMap,GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT);
		//内容分页表
		String contentPageTableName = findTableNameByType(tableMap,GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_PAGE);
		//内容评论表
		String commentTableName = findTableNameByType(tableMap,GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_COMMENT);
		//内容资源
		String contentResourceTableName = findTableNameByType(tableMap,GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_RESOURCE);
		
		String ky = "0";
		if(StringUtils.isNotBlank(primaryTableName) && StringUtils.isNotBlank(contentTableName)){
			//如果没有单独分页表
			if(StringUtils.isBlank(contentPageTableName)){
				dealWithContentPageation(contentBean,rule);
			}
			//生成主键
			ky = createPrimaryKey(primaryTableName,rule,contentBean,ky);
			if("-1".equals(ky)){
				return;
			}
			if(!primaryTableName.equals(contentTableName)){
				//插入内容
				insertValue(contentTableName,rule,contentBean,ky);
			}
			if(StringUtils.isNotBlank(commentTableName)){
				//插入评论
				insertLisValue(commentTableName,rule,contentBean,ky,contentBean.getCommentList());
				tableMap.remove(commentTableName);
			}
			if(StringUtils.isNotBlank(contentPageTableName)){
				//插入内容分页
				insertLisValue(contentPageTableName,rule,contentBean,ky,contentBean.getContentList());
				tableMap.remove(contentPageTableName);
			}
			if(StringUtils.isNotBlank(contentResourceTableName)){
				//插入资源
				insertResValue(contentResourceTableName,rule,contentBean,ky,contentBean.getResCrawlURIList());
				tableMap.remove(contentResourceTableName);
			}
			tableMap.remove(contentTableName);
			tableMap.remove(primaryTableName);
		}
		
		if(!tableMap.isEmpty()){
			String temp = "";
			for(Iterator<String> it = tableMap.keySet().iterator();it.hasNext();){
				temp = it.next();
				log.info("表名："+temp);
				insertValue(temp,rule,contentBean,ky);
			}
		}
	}
	
	/**
	 * 处理内容分页
	 * <p>方法说明:</>
	 * <li>单表内容分页</li>
	 * @author DuanYong
	 * @since 2014-8-26 下午3:54:13
	 * @version 1.0
	 * @exception 
	 * @param contentBean 内容值对象
	 * @param rule 采集规则对象
	 */
	protected void dealWithContentPageation(ContentBean contentBean,CrawlerRuleBean rule){
		if(StringUtils.isNotBlank(rule.getRuleDataBaseBean().getContentPageTag()) && !contentBean.getContentList().isEmpty()){
			StringBuilder str = new StringBuilder();
			str.append(contentBean.getContent());
			for(String content : contentBean.getContentList()){
				str.append(rule.getRuleDataBaseBean().getContentPageTag()).append(content);
			}
			contentBean.setContent(str.toString());
		}
	}
	/**
	 * 插入集合值
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午9:53:02
	 * @version 1.0
	 * @param tableName 表名
	 * @param rule 规则对象
	 * @param contentBean 内容值对象
	 * @param ky 主键
	 * @param contentList 内容、集合
	 */
	private void insertLisValue(String tableName,CrawlerRuleBean rule,ContentBean contentBean,String ky,List<String> contentList){
		PreparedStatement pstm = null; 
		Connection conn = null;
		String dataBaseId = rule.getRuleDataBaseBean().getDataBaseId();
		Map<String, ColumnBean> columnMap = rule.getRuleDataBaseBean().getHasSelectedValueMap().get(tableName);
		try{
			conn = this.connectionManager.getConnection(dataBaseId);
			if(CollectionUtils.isNotEmpty(contentList)){
				for(String content : contentList){
					int pstmKy = 1;
					pstm = conn.prepareStatement(this.populateSql(tableName,columnMap),Statement.RETURN_GENERATED_KEYS);
					ColumnBean columnBean = null;
					for(Iterator<String> it = columnMap.keySet().iterator();it.hasNext();){
						columnBean = columnMap.get(it.next());
						setValueByType(contentBean,rule,pstm,columnBean.getColumnType(),pstmKy++,columnBean.getColumnValue(),columnBean.getColumnValueType(),ky,content);
					}
					pstm.execute();
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			this.connectionManager.freeConnection(dataBaseId, conn);
			try {
				if(null != pstm){
					pstm.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 插入资源内容
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午9:56:41
	 * @version 1.0
	 * @param tableName 表名
	 * @param rule 规则对象
	 * @param contentBean 内容值对象
	 * @param ky 主键
	 * @param contentList 内容、集合
	 */
	private void insertResValue(String tableName,CrawlerRuleBean rule,ContentBean contentBean,String ky,List<CrawlResURI> contentList){
		PreparedStatement pstm = null; 
		Connection conn = null;
		String dataBaseId = rule.getRuleDataBaseBean().getDataBaseId();
		Map<String, ColumnBean> columnMap = rule.getRuleDataBaseBean().getHasSelectedValueMap().get(tableName);
		try{
			conn = this.connectionManager.getConnection(dataBaseId);
			if(CollectionUtils.isNotEmpty(contentList)){
				for(CrawlResURI res : contentList){
					int pstmKy = 1;
					pstm = conn.prepareStatement(this.populateSql(tableName,columnMap),Statement.RETURN_GENERATED_KEYS);
					ColumnBean columnBean = null;
					String value = "";
					for(Iterator<String> it = columnMap.keySet().iterator();it.hasNext();){
						columnBean = columnMap.get(it.next());
						if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_RESOURCE.equals(columnBean.getColumnValue())){
							value = res.getNewResUrl();
						}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_RESOURCE_NAME.equals(columnBean.getColumnValue())){
							value = res.getName();
						}else if(GatherConstant.DATA_BASE_SETTING_VALUES_KRY_CONTENT_RESOURCE_DESC.equals(columnBean.getColumnValue())){
							value = res.getDesc();
						}
						setValueByType(contentBean,rule,pstm,columnBean.getColumnType(),pstmKy++,columnBean.getColumnValue(),columnBean.getColumnValueType(),ky,value);
					}
					pstm.execute();
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			this.connectionManager.freeConnection(dataBaseId, conn);
			try {
				if(null != pstm){
					pstm.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 组装SQL语句
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午9:57:10
	 * @version 1.0
	 * @param tableName 表名
	 * @param columnMap 列Map对象
	 * @return SQL
	 */
	protected String populateSql(String tableName,Map<String, ColumnBean> columnMap){
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ").append(tableName); 
		sql.append(" (");
		int i = 0;
		for(Iterator<String> it = columnMap.keySet().iterator();it.hasNext();){
			if(i < columnMap.keySet().size() - 1){
				sql.append(it.next()+",");
			}else{
				sql.append(it.next());
			}
			i++;
		}
		sql.append(")");
		sql.append("VALUES (");
		i = 0;
		for(Iterator<String> it = columnMap.keySet().iterator();it.hasNext();){
			it.next();
			if(i < columnMap.keySet().size() - 1){
				sql.append("?,");
			}else{
				sql.append("?");
			}
			i++;
		}
		sql.append(")");
		log.info("=======SQL:"+sql);
		return sql.toString();
	} 
	/**
	 * 根据类型找表名
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午9:58:29
	 * @version 1.0
	 * @param tableMap 表MAP对象
	 * @param teblaType 表类型
	 * @return 表名
	 */
	private String findTableNameByType(Map<String,Map<String,ColumnBean>> tableMap,String teblaType){
		String table = "";
		//Map<String,Map<String,ColumnBean>> tableMap = rule.getRuleDataBaseBean().getHasSelectedValueMap();
		Map<String,ColumnBean> columnMap = null;
		boolean isFind = false;
		for(Iterator<String> it = tableMap.keySet().iterator();it.hasNext();){
			table = it.next();
			columnMap = tableMap.get(table);
			for(Iterator<String> column = columnMap.keySet().iterator();column.hasNext();){
				ColumnBean columnBean = columnMap.get(column.next());
				if(teblaType.equals(columnBean.getColumnValue())){
					isFind = true;
					break;
				}
			}
			if(isFind){
				break;
			}else{
				table = "";
			}
		}
		return table;
	}
	/**
	 * 根据字段类型设置值
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午9:59:13
	 * @version 1.0
	 * @param contentBean 内容对象
	 * @param rule 规则对象
	 * @param pstm PreparedStatement
	 * @param type 类型
	 * @param pos 位置
	 * @param value 值
	 * @param valueType 值类型
	 * @param ky 主键
	 * @param staticValue 静态值
	 */
	protected abstract void setValueByType(ContentBean contentBean,CrawlerRuleBean rule,PreparedStatement pstm,String type,int pos,String value,String valueType,String ky,String staticValue);
	/**
	 * 取得MAP静态值随机值
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2014-9-1 下午5:21:48
	 * @version 1.0
	 * @exception 
	 * @param value 值
	 * @param type 类型
	 * @return 值
	 */
	protected String getMapRandomValue(String value,String type){
		String key = String.valueOf(value.hashCode());
		if(!randomValueMap.containsKey(key)){
			List<SelectValueBean> staticValueList = new ArrayList<SelectValueBean>();
			if(StringUtils.isNotBlank(value)){
				List tempList = (ArrayList)JsonUtils.formatStringToObject(value, ArrayList.class);
				for(Object obj : tempList){
					staticValueList.add((SelectValueBean)JsonUtils.formatStringToObject(obj.toString(), SelectValueBean.class));
				}
			}
			if(!CollectionUtils.isEmpty(staticValueList)){
				randomValueMap.put(key, staticValueList.get((int)(Math.random()*(staticValueList.size()))+0));	
			}
		}
		if(!randomValueMap.isEmpty()){
			if(GatherConstant.EXTEND_FIELDS_VALUE_TYPE_STATIC_MAP_VALUE.equals(type)){
				return randomValueMap.get(key).getValue();
			}
			return randomValueMap.get(key).getValueName();
		}
		return value;
	}
	/**
	 * 取得简单值
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2014-9-1 下午5:37:53
	 * @version 1.0
	 * @exception 
	 * @param value 值
	 * @return
	 */
	protected String getRandomValue(String value){
		if(!value.contains(",")){
			return value;
		}
		String key = String.valueOf(value.hashCode());
		if(!randomValue.containsKey(key)){
			//如果值是以逗号分隔 则随机取其中一个数据
			if(StringUtils.isNotBlank(value)){
				String[] tempValues = value.split(",");
				if(tempValues.length > 1){
					value = tempValues[(int)(Math.random()*(tempValues.length))+0];
				}else{
					value = tempValues[0];
				}
				tempValues = null;
			}
			randomValue.put(key, value);
		}
		if(!randomValue.isEmpty()){
			return randomValue.get(key);
		}
		return value;
	}
	
	/**
	 * 生成主键
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午10:01:14
	 * @version 1.0
	 * @param tableName 表名
	 * @param rule 规则对象
	 * @param contentBean 值对象
	 * @param ky 主键
	 * @return 主键
	 */
	private String createPrimaryKey(String tableName,CrawlerRuleBean rule,ContentBean contentBean,String ky){
		if(GatherConstant.PK_GEN_TABLE.equals(rule.getRuleDataBaseBean().getPrimaryGen())){
			ky = pkGenTable(rule);
			//插入主表数据
			insertPrimaryTable(tableName,rule,contentBean,ky);
			return ky;
		}else if(GatherConstant.PK_GEN_UUID.equals(rule.getRuleDataBaseBean().getPrimaryGen())){
			ky = pkGenUuid();
			//插入主表数据
			insertPrimaryTable(tableName,rule,contentBean,ky);
			return ky;
		}
		return pkGenAuto(tableName, rule, contentBean, ky);
	}
	/**
	 * 插入主表
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午10:01:59
	 * @version 1.0
	 * @param tableName 表名
	 * @param rule 规则对象
	 * @param contentBean 内容对象
	 * @param ky 主键
	 */
    private void insertPrimaryTable(String tableName, CrawlerRuleBean rule,
			ContentBean contentBean, String ky) {
    	PreparedStatement pstm = null; 
		Connection conn = null;
		ResultSet rs = null;
		String dataBaseId = rule.getRuleDataBaseBean().getDataBaseId();
		Map<String, ColumnBean> columnMap = rule.getRuleDataBaseBean().getHasSelectedValueMap().get(tableName);
		int pstmKy = 1;
		try{
			conn = this.connectionManager.getConnection(dataBaseId);
			pstm = conn.prepareStatement(this.populateSql(tableName,columnMap),Statement.RETURN_GENERATED_KEYS);
			ColumnBean columnBean = null;
			for(Iterator<String> it = columnMap.keySet().iterator();it.hasNext();){
				columnBean = columnMap.get(it.next());
				setValueByType(contentBean,rule,pstm,columnBean.getColumnType(),pstmKy++,columnBean.getColumnValue(),columnBean.getColumnValueType(),ky,"");
			}
			pstm.execute();
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			this.connectionManager.freeConnection(dataBaseId, conn);
			try {
				if(null != pstm){
					pstm.close();
				}
				if(null != rs){
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
    /**
     * 生成UUID
     * <p>
     * 说明:
     * </p>
     * <li></li>
     * @author DuanYong
     * @since 2016-1-26上午10:02:59
     * @version 1.0
     * @return
     */
	private String pkGenUuid() {
		return UUID.randomUUID().toString();
	}
    /**
     * 按主键表生成主键
     * <p>
     * 说明:
     * </p>
     * <li></li>
     * @author DuanYong
     * @since 2016-1-26上午10:03:18
     * @version 1.0
     * @param rule
     * @return
     */
	private String pkGenTable(CrawlerRuleBean rule) {
		PreparedStatement pstm = null; 
		Connection conn = null;
		ResultSet rs = null;
		String dataBaseId = rule.getRuleDataBaseBean().getDataBaseId();
		int keyValue = -1;
		try{
			conn = this.connectionManager.getConnection(dataBaseId);
			pstm = conn.prepareStatement(this.updatePkGenTableSql(rule),Statement.RETURN_GENERATED_KEYS);
			pstm.execute();
			pstm = conn.prepareStatement(this.populateTableGenKeySql(rule),Statement.RETURN_GENERATED_KEYS);
			pstm.execute();
			rs = pstm.getResultSet();
			if (rs.next()) {
			    keyValue = rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			this.connectionManager.freeConnection(dataBaseId, conn);
			try {
				if(null != pstm){
					pstm.close();
				}
				if(null != rs){
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return String.valueOf(keyValue);
	}
    /**
     * 组装按表生成主键SQL
     * <p>
     * 说明:
     * </p>
     * <li></li>
     * @author DuanYong
     * @since 2016-1-26上午10:03:35
     * @version 1.0
     * @param rule
     * @return
     */
	private String populateTableGenKeySql(CrawlerRuleBean rule) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ").append(rule.getRuleDataBaseBean().getPrimaryGenPkValueColumnName())
		.append(" FROM ").append(rule.getRuleDataBaseBean().getPrimaryGenTable())
		.append(" WHERE ").append(rule.getRuleDataBaseBean().getPrimaryGenTableColumnName())
		.append(" = '").append(rule.getRuleDataBaseBean().getPrimaryGenTableColumnValue()).append("'"); 
		log.info("======gen pk SQL:"+sql);
		return sql.toString();
	}
	/**
	 * 更新主键表SQL
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午10:04:03
	 * @version 1.0
	 * @param rule
	 * @return
	 */
	private String updatePkGenTableSql(CrawlerRuleBean rule) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(rule.getRuleDataBaseBean().getPrimaryGenTable())
		.append(" SET ").append(rule.getRuleDataBaseBean().getPrimaryGenPkValueColumnName())
		.append(" = ").append(rule.getRuleDataBaseBean().getPrimaryGenPkValueColumnName()).append(" + 1")
		.append(" WHERE ").append(rule.getRuleDataBaseBean().getPrimaryGenTableColumnName())
		.append(" = '").append(rule.getRuleDataBaseBean().getPrimaryGenTableColumnValue()).append("'"); 
		log.info("======UPDATE pk SQL:"+sql);
		return sql.toString();
	}

	/**
     * 自动生成主键
     * <p>
     * 说明:
     * </p>
     * <li></li>
     * @author DuanYong
     * @since 2016-1-22下午12:28:45
     * @version 1.0
     * @param tableName 表名
     * @param rule 规则
     * @param contentBean 内容
     * @param ky 主键
     * @return 主键
     */
	private String pkGenAuto(String tableName, CrawlerRuleBean rule,
			ContentBean contentBean, String ky) {
		PreparedStatement pstm = null; 
		Connection conn = null;
		ResultSet rs = null;
		String dataBaseId = rule.getRuleDataBaseBean().getDataBaseId();
		Map<String, ColumnBean> columnMap = rule.getRuleDataBaseBean().getHasSelectedValueMap().get(tableName);
		int keyValue = -1;
		int pstmKy = 1;
		try{
			conn = this.connectionManager.getConnection(dataBaseId);
			pstm = conn.prepareStatement(this.populateSql(tableName,columnMap),Statement.RETURN_GENERATED_KEYS);
			ColumnBean columnBean = null;
			for(Iterator<String> it = columnMap.keySet().iterator();it.hasNext();){
				columnBean = columnMap.get(it.next());
				setValueByType(contentBean,rule,pstm,columnBean.getColumnType(),pstmKy++,columnBean.getColumnValue(),columnBean.getColumnValueType(),ky,"");
			}
			pstm.execute();
			rs = pstm.getGeneratedKeys();
			if (rs.next()) {
			    keyValue = rs.getInt(1);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			this.connectionManager.freeConnection(dataBaseId, conn);
			try {
				if(null != pstm){
					pstm.close();
				}
				if(null != rs){
					rs.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return String.valueOf(keyValue);
	}
	/**
	 * 插入值
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午10:04:42
	 * @version 1.0
     * @param tableName 表名
     * @param rule 规则
     * @param contentBean 内容
     * @param ky 主键
	 */
	private void insertValue(String tableName,CrawlerRuleBean rule,ContentBean contentBean,String ky){
		PreparedStatement pstm = null; 
		Connection conn = null;
		String dataBaseId = rule.getRuleDataBaseBean().getDataBaseId();
		Map<String, ColumnBean> columnMap = rule.getRuleDataBaseBean().getHasSelectedValueMap().get(tableName);
		int pstmKy = 1;
		try{
			conn = this.connectionManager.getConnection(dataBaseId);
			pstm = conn.prepareStatement(this.populateSql(tableName,columnMap),Statement.RETURN_GENERATED_KEYS);
			//是否有一对多，保存扩展字段的情况
			List<Map<String, ColumnBean>> newColumnMapLis = getNewColumnBeanMapList(columnMap);
			ColumnBean columnBean = null;
			if(newColumnMapLis.isEmpty()){
				for(Iterator<String> it = columnMap.keySet().iterator();it.hasNext();){
					columnBean = columnMap.get(it.next());
					setValueByType(contentBean,rule,pstm,columnBean.getColumnType(),pstmKy++,columnBean.getColumnValue(),columnBean.getColumnValueType(),ky,"");
				}
				pstm.execute();
			}else{
				for(Map<String, ColumnBean> newColumnMap : newColumnMapLis){
					pstmKy = 1;
					for(Iterator<String> it = newColumnMap.keySet().iterator();it.hasNext();){
						columnBean = newColumnMap.get(it.next());
						setValueByType(contentBean,rule,pstm,columnBean.getColumnType(),pstmKy++,columnBean.getColumnValue(),columnBean.getColumnValueType(),ky,"");
					}
					pstm.execute();
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			this.connectionManager.freeConnection(dataBaseId, conn);
			try {
				if(null != pstm){
					pstm.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 取得新的字段MAP对象集合
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2014-9-26 下午10:06:31
	 * @version 1.0
	 * @exception 
	 * @param columnMap
	 * @return
	 */
	private List<Map<String, ColumnBean>> getNewColumnBeanMapList(Map<String, ColumnBean> columnMap){
		//新字段对象集合
		List<Map<String, ColumnBean>> newColumnMapLis = new ArrayList<Map<String, ColumnBean>>();
		ColumnBean columnBean = null;
		//字段值中最大长度
		int maxLen = 0;
		//字段值中最大长度时 字段名称
		String maxColumnName = "";
		//单值MAP对象
		Map<String, ColumnBean> columnSingleValueMap = new HashMap<String, ColumnBean>();
		//多值MAP对象
		Map<String,ColumnBean> columnMultValueMap = new HashMap<String,ColumnBean>();
		//多值MAP与字段名称对照
		Map<String,String[]> columnMultValuesMap = new HashMap<String,String[]>();
		for(String key : columnMap.keySet()){
			columnBean = columnMap.get(key);
			if((GatherConstant.EXTEND_FIELDS_VALUE_TYPE_DYNAMIC_MAP_KEY.equals(columnBean.getColumnValueType()) || GatherConstant.EXTEND_FIELDS_VALUE_TYPE_DYNAMIC_MAP_VALUE.equals(columnBean.getColumnValueType())) && columnBean.getColumnValue().contains(",")){
				columnMultValueMap.put(key, columnBean);
				columnMultValuesMap.put(key, columnBean.getColumnValue().split(","));
				if(maxLen < (columnBean.getColumnValue().split(",")).length){
					maxLen = (columnBean.getColumnValue().split(",")).length;
					maxColumnName = columnBean.getColumnName();
				}
			}else{
				columnSingleValueMap.put(key, columnBean);
			}
		}
		//如果有多值
		if(!columnMultValueMap.isEmpty()){
			Map<String, ColumnBean> newClumnMap = null;
			ColumnBean newColumnBean = null;
			ColumnBean maxColumnBean = columnMultValueMap.get(maxColumnName);
			columnMultValueMap.remove(maxColumnName);
			//则按最长字段值 来重新组合
			for(int i = 0 ; i < maxLen ;i++){
				newClumnMap = new HashMap<String, ColumnBean>();
				newClumnMap.putAll(columnSingleValueMap);
				//设置最大字段值
				newColumnBean = new ColumnBean();
				BeanUtils.copyProperties(maxColumnBean, newColumnBean);
				newColumnBean.setColumnValue(columnMultValuesMap.get(maxColumnName)[i]);
				newClumnMap.put(maxColumnName, newColumnBean);
				//设置其他多值字段
				for(String key : columnMultValueMap.keySet()){
					newColumnBean = new ColumnBean();
					BeanUtils.copyProperties(columnMap.get(key), newColumnBean);
					if(null == columnMultValuesMap.get(key) || null == columnMultValuesMap.get(key)[i]){
						newColumnBean.setColumnValue("");
					}else{
						newColumnBean.setColumnValue(columnMultValuesMap.get(key)[i]);
					}
					newClumnMap.put(key, newColumnBean);
				}
				newColumnMapLis.add(newClumnMap);
			}
		}
		return newColumnMapLis;
	}
	/**
	 * 获取随机时间
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-1-26上午10:05:17
	 * @version 1.0
	 * @param rule 规则
	 * @return 随机时间
	 */
	protected Date getRandomDate(CrawlerRuleBean rule){
		Date viewDate = new Date();
		if(Boolean.valueOf(rule.getRuleBaseBean().getRandomDateFlag())){
			String format = StringUtils.isNotBlank(rule.getRuleBaseBean().getDateFormat()) ? rule.getRuleBaseBean().getDateFormat() : "yyyy-MM-dd";
			SimpleDateFormat dt = new SimpleDateFormat(format);
			SimpleDateFormat dft = new SimpleDateFormat("yyyy-MM-dd");
			String statrDate = StringUtils.isNotBlank(rule.getRuleBaseBean().getStartRandomDate()) ? rule.getRuleBaseBean().getStartRandomDate() : Constant.DEFAULT_START_DATE;
			String endDate = StringUtils.isNotBlank(rule.getRuleBaseBean().getEndRandomDate()) ? rule.getRuleBaseBean().getEndRandomDate() : dft.format(new java.util.Date());
			viewDate = DateFormatUtils.randomDate(statrDate, endDate);
			String tempDate = dt.format(viewDate);
			viewDate = DateUtil.strToDate(tempDate,format);
		}
		return viewDate;
	}
	
	public static void main(String[] args){
		String value = "1,2";
		if(StringUtils.isNotBlank(value) && value.contains(",")){
			String[] tempValues = value.split(",");
			if(tempValues.length > 1){
				value = tempValues[(int)(Math.random()*(tempValues.length))+0];
			}else{
				value = tempValues[0];
			}
			tempValues = null;
		}
		System.out.println(value);
	}


}
