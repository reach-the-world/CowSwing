/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.javacoo.cowswing.base.service.ICrawlerService;
import org.javacoo.cowswing.core.event.CowSwingEvent;
import org.javacoo.cowswing.core.event.CowSwingEventType;
import org.javacoo.cowswing.core.event.CowSwingObserver;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCommentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerContentPaginationCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerExtendFieldCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerRuleCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskBean;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerTaskCriteria;
import org.javacoo.cowswing.plugin.gather.service.beans.RuleDataHandleConfigBean;
import org.javacoo.cowswing.plugin.gather.service.export.ExportHelper;
import org.javacoo.cowswing.plugin.gather.service.export.bean.ExportBean;
import org.javacoo.crawler.core.data.ContentBean;
import org.javacoo.crawler.core.data.CrawlerExtendFieldBean;
import org.javacoo.crawler.core.util.File.FileHelper;

/**
 * 数据处理
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-18下午4:59:40
 * @version 1.0
 */
public class DataHandle extends AbstractThreadService{
	/**数据导出帮助类*/
	private ExportHelper exportHelper;
	/**文件帮助类*/
	private FileHelper fileHelper;
	/**规则服务类*/
	private ICrawlerService<CrawlerRuleBean,CrawlerRuleCriteria> crawlerRuleService;
	/**采集内容服务类*/
	protected ICrawlerService<CrawlerContentBean,CrawlerContentCriteria> crawlerContentService;
	/**采集内容评论服务类*/
	protected ICrawlerService<CrawlerContentCommentBean,CrawlerContentCommentCriteria> crawlerContentCommentService;
	/**采集内容分页服务类*/
	protected ICrawlerService<CrawlerContentPaginationBean,CrawlerContentPaginationCriteria> crawlerContentPaginationService;
	/**扩展字段服务类*/
	protected ICrawlerService<CrawlerExtendFieldBean,CrawlerExtendFieldCriteria> crawlerExtendFieldService;
	/**规则ID*/
	private Integer ruleId;
	public DataHandle(Integer ruleId,
			ExportHelper exportHelper,
			FileHelper fileHelper,
			ICrawlerService<CrawlerRuleBean, CrawlerRuleCriteria> crawlerRuleService,
			ICrawlerService<CrawlerContentBean, CrawlerContentCriteria> crawlerContentService,
			ICrawlerService<CrawlerContentCommentBean, CrawlerContentCommentCriteria> crawlerContentCommentService,
			ICrawlerService<CrawlerContentPaginationBean, CrawlerContentPaginationCriteria> crawlerContentPaginationService,
			ICrawlerService<CrawlerExtendFieldBean, CrawlerExtendFieldCriteria> crawlerExtendFieldService,
			ICrawlerService<CrawlerTaskBean,CrawlerTaskCriteria> crawlerTaskService){
		super(crawlerTaskService);
		this.ruleId = ruleId;
		this.exportHelper = exportHelper;
		this.fileHelper = fileHelper;
		this.crawlerRuleService = crawlerRuleService;
		this.crawlerContentService = crawlerContentService;
		this.crawlerContentCommentService = crawlerContentCommentService;
		this.crawlerContentPaginationService = crawlerContentPaginationService;
		this.crawlerExtendFieldService = crawlerExtendFieldService;
		this.crawlerTaskService = crawlerTaskService;
	}
	/**
	 * 执行处理
	 */
	protected void doRun(){
		CrawlerRuleBean rule = new CrawlerRuleBean();
		rule.setRuleId(this.ruleId);
		rule = this.crawlerRuleService.get(rule, GatherConstant.SQLMAP_ID_GET_CRAWLER_RULE);
		boolean hasNotify = false;
		if(null != rule.getRuleDataHandleConfigBean() && Boolean.valueOf(rule.getRuleDataHandleConfigBean().getUseExportFlag())){
			CrawlerContentCriteria crawlerContentCriteria = new CrawlerContentCriteria();
			crawlerContentCriteria.setRuleId(ruleId);
			List<CrawlerContentBean> resultList = this.crawlerContentService.getList(crawlerContentCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT);
			if(CollectionUtils.isNotEmpty(resultList)){
				hasNotify = true;
				List<ContentBean> contentBeanList = preExport(resultList);
				doExport(rule, contentBeanList);
			}
		}

		if(!hasNotify){
			CowSwingObserver.getInstance().notifyEvents(new CowSwingEvent(this,CowSwingEventType.DataHandleFinishedEvent,ruleId,""));
		}
	}
	
	/**
	 * 执行数据导出
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月26日下午4:09:27
	 * @param rule
	 * @param resultList
	 */
	private void doExport(CrawlerRuleBean rule, List<ContentBean> resultList) {
		//插入任务
		insertTask(ruleId,resultList.size(),GatherConstant.TASK_TYPE_5,CowSwingEventType.DataHandleStartEvent);
		//休眠，避免锁表
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		if(Boolean.valueOf(rule.getRuleDataHandleConfigBean().getExportListFlag())){
			//内容处理
			handleList(resultList,rule);
		}else{
			String desc = "";
			for(ContentBean contentBean : resultList){
				//内容处理
				handleContent(contentBean,rule);
				desc = "处理内容："+contentBean.getTitle()+",保存至指定目录 成功!";
				updateTask(this.ruleId,desc,GatherConstant.TASK_TYPE_5,CowSwingEventType.DataHandleStatusChangeEvent);
			}
		}
		
		deleteTask(this.ruleId,GatherConstant.TASK_TYPE_5,LanguageLoader.getString("CrawlerMainFrame.CrawlSaveRemoteComplateTask"),CowSwingEventType.DataHandleFinishedEvent);
	}
	
	/**
	 * 数据导出前处理
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月26日下午4:17:45
	 * @param targetList
	 * @return
	 */
	private List<ContentBean> preExport(List<CrawlerContentBean> targetList){
		List<ContentBean> resultList = new ArrayList<ContentBean>();
		ContentBean contentBean = null;
		List<CrawlerContentCommentBean> commentList = null;
		CrawlerContentCommentCriteria crawlerContentCommentCriteria = new CrawlerContentCommentCriteria();
		
		List<CrawlerContentPaginationBean> contentPageList = null;
		CrawlerContentPaginationCriteria crawlerContentPaginationCriteria = new CrawlerContentPaginationCriteria();

		
		List<CrawlerExtendFieldBean> extendFieldList = null;
		CrawlerExtendFieldCriteria crawlerExtendFieldCriteria = new CrawlerExtendFieldCriteria();
		for(CrawlerContentBean crawlerContentBean : targetList){
			contentBean = new ContentBean();
			contentBean.setBrief(crawlerContentBean.getBrief());
			contentBean.setContent(crawlerContentBean.getContent());
			contentBean.setContentPlainText(crawlerContentBean.getContentPlainText());
			contentBean.setTitle(crawlerContentBean.getTitle());
			contentBean.setTitleImg(crawlerContentBean.getTitleImg());
			contentBean.setUrl(crawlerContentBean.getUrl());
			
			crawlerContentCommentCriteria.setContentId(crawlerContentBean.getContentId());
			commentList = this.crawlerContentCommentService.getList(crawlerContentCommentCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_COMMENT);
			if(CollectionUtils.isNotEmpty(commentList)){
				for(CrawlerContentCommentBean crawlerContentCommentBean : commentList){
					contentBean.getCommentList().add(crawlerContentCommentBean.getContent());
				}
			}
			crawlerContentPaginationCriteria.setContentId(crawlerContentBean.getContentId());
			contentPageList = this.crawlerContentPaginationService.getList(crawlerContentPaginationCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_CONTENT_PAGINATION);
			if(CollectionUtils.isNotEmpty(contentPageList)){
				for(CrawlerContentPaginationBean crawlerContentPaginationBean : contentPageList){
					contentBean.getContentList().add(crawlerContentPaginationBean.getContent());
				}
			}
			
			crawlerExtendFieldCriteria.setContentId(crawlerContentBean.getContentId());
			extendFieldList = this.crawlerExtendFieldService.getList(crawlerExtendFieldCriteria, GatherConstant.SQLMAP_ID_GET_LIST_CRAWLER_EXTEND_FIELD);
			if(CollectionUtils.isNotEmpty(extendFieldList)){
				for(CrawlerExtendFieldBean crawlerExtendFieldBean : extendFieldList){
					contentBean.getFieldValueMap().put(crawlerExtendFieldBean.getFieldName(), crawlerExtendFieldBean.getFieldValue());
				}
			}
			resultList.add(contentBean);
		}
		return resultList;
	}
	/**
	 * 内容处理
	 * <p>
	 * 说明:
	 * </p>
	 * <li></li>
	 * @author DuanYong
	 * @since 2016-2-19上午9:10:47
	 * @version 1.0
	 * @param contentBean 内容
	 * @param rule 规则
	 */
	private void handleContent(ContentBean contentBean, CrawlerRuleBean rule) {
		//获取数据处理参数对象 
		RuleDataHandleConfigBean ruleDataHandleConfigBean = rule.getRuleDataHandleConfigBean();
		//组装数据
		Map<Object, Object> dataMap = new HashMap<Object, Object>();
		contentBean.setCharset(rule.getRuleBaseBean().getPageEncoding());
		dataMap.put("data", contentBean);
		//生成数据导出对象
		ExportBean eb = this.exportHelper.generateExportBean(
				ruleDataHandleConfigBean.getUseExportType(), 
				contentBean.getTitle(), 
				ruleDataHandleConfigBean.getTemplateName(), 
				dataMap);
		
		//保存文件
		fileHelper.saveFile(IOUtils.toInputStream(eb.getContent()), ruleDataHandleConfigBean.getSavePath()+eb.getFileName());
	}
	/**
	 * 处理集合内容
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月26日下午4:30:06
	 * @param resultList
	 * @param rule
	 */
	private void handleList(List<ContentBean> resultList, CrawlerRuleBean rule) {
		//获取数据处理参数对象 
		RuleDataHandleConfigBean ruleDataHandleConfigBean = rule.getRuleDataHandleConfigBean();
		//组装数据
		Map<Object, Object> dataMap = new HashMap<Object, Object>();
		dataMap.put("dataList", resultList);
		//生成数据导出对象
		ExportBean eb = this.exportHelper.generateExportBean(
				ruleDataHandleConfigBean.getUseExportType(), 
				rule.getRuleName(), 
				ruleDataHandleConfigBean.getTemplateName(), 
				dataMap);
		
		//保存文件
		fileHelper.saveFile(IOUtils.toInputStream(eb.getContent()), ruleDataHandleConfigBean.getSavePath()+eb.getFileName());
	}

}
