package org.javacoo.cowswing.plugin.gather.service.beans;

/**
 * 采集评论参数值对象
 *@author DuanYong
 *@since 2012-11-15上午9:32:44
 *@version 1.0
 */
public class RuleCommentBean {
	/**评论列表入口开始标签*/
	private java.lang.String commentIndexStart;
	/**评论列表入口结束标签*/
	private java.lang.String commentIndexEnd;
	/**评论列表入口内容处理字符串*/
	private java.lang.String commentIndexContentHandleStr;
	/**评论内容区域开始标签*/
	private java.lang.String commentAreaStart;
	/**评论内容区域结束标签*/
	private java.lang.String commentAreaEnd;
	/**评论内容区域内容处理字符串*/
	private java.lang.String commentAreaContentHandleStr;
	/**评论开始标签*/
	private java.lang.String commentStart;
	/**评论结束标签*/
	private java.lang.String commentEnd;
	/**评论开始内容处理字符串*/
	private java.lang.String commentContentHandleStr;
	/**评论连接开始标签*/
	private java.lang.String commentLinkStart;
	/**评论连接结束标签*/
	private java.lang.String commentLinkEnd;
	/**评论连接结束内容处理字符串*/
	private java.lang.String commentLinkContentHandleStr;
	public java.lang.String getCommentStart() {
		return commentStart;
	}
	public void setCommentStart(java.lang.String commentStart) {
		this.commentStart = commentStart;
	}
	public java.lang.String getCommentIndexStart() {
		return commentIndexStart;
	}
	public void setCommentIndexStart(java.lang.String commentIndexStart) {
		this.commentIndexStart = commentIndexStart;
	}
	public java.lang.String getCommentIndexEnd() {
		return commentIndexEnd;
	}
	public void setCommentIndexEnd(java.lang.String commentIndexEnd) {
		this.commentIndexEnd = commentIndexEnd;
	}
	public java.lang.String getCommentAreaStart() {
		return commentAreaStart;
	}
	public void setCommentAreaStart(java.lang.String commentAreaStart) {
		this.commentAreaStart = commentAreaStart;
	}
	public java.lang.String getCommentAreaEnd() {
		return commentAreaEnd;
	}
	public void setCommentAreaEnd(java.lang.String commentAreaEnd) {
		this.commentAreaEnd = commentAreaEnd;
	}
	public java.lang.String getCommentEnd() {
		return commentEnd;
	}
	public void setCommentEnd(java.lang.String commentEnd) {
		this.commentEnd = commentEnd;
	}
	public java.lang.String getCommentLinkStart() {
		return commentLinkStart;
	}
	public void setCommentLinkStart(java.lang.String commentLinkStart) {
		this.commentLinkStart = commentLinkStart;
	}
	public java.lang.String getCommentLinkEnd() {
		return commentLinkEnd;
	}
	public void setCommentLinkEnd(java.lang.String commentLinkEnd) {
		this.commentLinkEnd = commentLinkEnd;
	}
	public java.lang.String getCommentIndexContentHandleStr() {
		return commentIndexContentHandleStr;
	}
	public void setCommentIndexContentHandleStr(
			java.lang.String commentIndexContentHandleStr) {
		this.commentIndexContentHandleStr = commentIndexContentHandleStr;
	}
	public java.lang.String getCommentAreaContentHandleStr() {
		return commentAreaContentHandleStr;
	}
	public void setCommentAreaContentHandleStr(
			java.lang.String commentAreaContentHandleStr) {
		this.commentAreaContentHandleStr = commentAreaContentHandleStr;
	}
	public java.lang.String getCommentContentHandleStr() {
		return commentContentHandleStr;
	}
	public void setCommentContentHandleStr(java.lang.String commentContentHandleStr) {
		this.commentContentHandleStr = commentContentHandleStr;
	}
	public java.lang.String getCommentLinkContentHandleStr() {
		return commentLinkContentHandleStr;
	}
	public void setCommentLinkContentHandleStr(
			java.lang.String commentLinkContentHandleStr) {
		this.commentLinkContentHandleStr = commentLinkContentHandleStr;
	}
	
	

}
