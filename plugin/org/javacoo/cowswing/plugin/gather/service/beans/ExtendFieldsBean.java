/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.service.beans;

import org.javacoo.crawler.core.constants.Constants;

/**
 * 扩展字段bean
 *@author DuanYong
 *@since 2013-3-18上午9:38:29
 *@version 1.0
 */
public class ExtendFieldsBean {
	/**扩展字段*/
	private String fields;
	/**扩展字段标签*/
	private String filterStart;
	/**扩展字段过滤标签*/
	private String filterEnd;
	/**扩展字段内容处理字符串*/
	private String filterContentHandleStr;
	/**过滤标签类型*/
	private String filterType = Constants.FILTER_TYPE_TAG;
	
	public String getFields() {
		return fields;
	}
	public void setFields(String fields) {
		this.fields = fields;
	}
	public String getFilterStart() {
		return filterStart;
	}
	public void setFilterStart(String filterStart) {
		this.filterStart = filterStart;
	}
	public String getFilterEnd() {
		return filterEnd;
	}
	public void setFilterEnd(String filterEnd) {
		this.filterEnd = filterEnd;
	}
	
	public String getFilterContentHandleStr() {
		return filterContentHandleStr;
	}
	public void setFilterContentHandleStr(String filterContentHandleStr) {
		this.filterContentHandleStr = filterContentHandleStr;
	}
	/**
	 * @return the filterType
	 */
	public String getFilterType() {
		return filterType;
	}
	/**
	 * @param filterType the filterType to set
	 */
	public void setFilterType(String filterType) {
		this.filterType = filterType;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ExtendFieldsBean [fields=" + fields + ", filterStart="
				+ filterStart + ", filterEnd=" + filterEnd + ", filterType="
				+ filterType + "]";
	}
	
	
	
	

}
