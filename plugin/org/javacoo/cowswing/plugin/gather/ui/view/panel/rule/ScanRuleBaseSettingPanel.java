package org.javacoo.cowswing.plugin.gather.ui.view.panel.rule;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.RuleBaseBean;
import org.javacoo.cowswing.ui.listener.IntegerVerifier;
import org.javacoo.cowswing.ui.listener.TextVerifier;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;
/**
 * 全站扫描基本参数设置panel
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2014-9-23 上午9:05:58
 * @version 1.0
 */
@Component("scanRuleBaseSettingPanel")
public class ScanRuleBaseSettingPanel extends AbstractContentPanel<RuleBaseBean>{
	private static final long serialVersionUID = 1L;
	/**采集名称输入框*/
	private javax.swing.JTextField siteField;
	/**采集名称标签*/
	private javax.swing.JLabel siteLabel;
	/**采集地址输入框*/
	private javax.swing.JTextField linkField;
	/**采集地址标签*/
	private javax.swing.JLabel linkLabel;
	/**页面编码输入框*/
	private javax.swing.JTextField pageEncodingField;
	/**页面编码标签*/
	private javax.swing.JLabel pageEncodingLabel;
	/**扫描深度输入框*/
	private javax.swing.JTextField depthField;
	/**扫描深度标签*/
	private javax.swing.JLabel depthLabel;
	/**暂停时间输入框*/
	private javax.swing.JTextField sleepTimeField;
	/**暂停时间标签*/
	private javax.swing.JLabel sleepTimeLabel;
	/**是否只扫描本站单选按钮*/
	private javax.swing.JRadioButton onlyScanSelfYesButton;
	/**是否只扫描本站单选按钮*/
	private javax.swing.JRadioButton onlyScanSelfNoButton;
	/**是否只扫描本站单选按钮组*/
	private javax.swing.ButtonGroup onlyScanSelfButtonGroup;
	/**是否只扫描本站标签*/
	private javax.swing.JLabel onlyScanSelfLabel;
	
	/**是否使用代理单选按钮*/
	private javax.swing.JRadioButton proxyYesButton;
	/**是否使用代理单选按钮*/
	private javax.swing.JRadioButton proxyNoButton;
	/**是否使用代理单选按钮组*/
	private javax.swing.ButtonGroup proxyButtonGroup;
	/**是否使用代理标签*/
	private javax.swing.JLabel proxyLabel;
	
	
	/**是否开启监控单选按钮*/
	private javax.swing.JRadioButton openMonitorYesButton;
	/**是否开启监控单选按钮*/
	private javax.swing.JRadioButton openMonitorNoButton;
	/**是否开启监控单选按钮组*/
	private javax.swing.ButtonGroup openMonitorButtonGroup;
	/**是否开启监控标签*/
	private javax.swing.JLabel openMonitorLabel;
	
	/**代理地址输入框*/
	private javax.swing.JTextField proxyAddressField;
	/**代理地址标签*/
	private javax.swing.JLabel proxyAddressLabel;
	/**端口入框*/
	private javax.swing.JTextField proxyPortField;
	/**端口标签*/
	private javax.swing.JLabel proxyPortLabel;
	
	
	
	
	
	private String onlyScanSelfValue;
	private String proxySelectValue;
	private String openMonitorValue;
	

	/**
	 * 初始化面板控件
	 */
	protected void initComponents(){
		useGridBagLayout();
		
		siteLabel = new javax.swing.JLabel();
		siteField = new javax.swing.JTextField();
		

		linkLabel = new javax.swing.JLabel();
		linkField = new javax.swing.JTextField();
		
		pageEncodingLabel = new javax.swing.JLabel();
		pageEncodingField = new javax.swing.JTextField();

		depthLabel = new javax.swing.JLabel();
		depthField = new javax.swing.JTextField();
		
		
		sleepTimeLabel = new javax.swing.JLabel();
		sleepTimeField = new javax.swing.JTextField();
		
		onlyScanSelfLabel = new javax.swing.JLabel();
		onlyScanSelfYesButton = new javax.swing.JRadioButton();
		onlyScanSelfNoButton = new javax.swing.JRadioButton();
		onlyScanSelfButtonGroup = new javax.swing.ButtonGroup();
		
		proxyLabel = new javax.swing.JLabel();
		proxyYesButton = new javax.swing.JRadioButton();
		proxyNoButton = new javax.swing.JRadioButton();
		proxyButtonGroup = new javax.swing.ButtonGroup();
		
		
		openMonitorLabel = new javax.swing.JLabel();
		openMonitorYesButton = new javax.swing.JRadioButton();
		openMonitorNoButton = new javax.swing.JRadioButton();
		openMonitorButtonGroup = new javax.swing.ButtonGroup();
		
		proxyAddressLabel = new javax.swing.JLabel();
		proxyAddressField = new javax.swing.JTextField();

		proxyPortLabel = new javax.swing.JLabel();
		proxyPortField = new javax.swing.JTextField();
		
		
		
		siteLabel.setText(LanguageLoader.getString("RuleContentSetting.name"));
		addCmpToGridBag(siteLabel,new GBC(0,0));

		siteField.setColumns(20);
		siteField.setInputVerifier(new TextVerifier(this,false));
		siteField.setText(LanguageLoader.getString("RuleContentSetting.nameDefaultValue"));
		addCmpToGridBag(siteField,new GBC(1,0,3,1));
		
		
		
		linkLabel.setText(LanguageLoader.getString("Scan.settingLink"));
		addCmpToGridBag(linkLabel,new GBC(0,1));

		linkField.setColumns(20);
		linkField.setInputVerifier(new TextVerifier(this,false));
		addCmpToGridBag(linkField,new GBC(1,1,3,1));
		
		
		sleepTimeLabel.setText(LanguageLoader.getString("RuleContentSetting.sleepTime"));
		addCmpToGridBag(sleepTimeLabel,new GBC(0,2));

		sleepTimeField.setColumns(20);
		sleepTimeField.setInputVerifier(new IntegerVerifier(this, false, 1, 10000));
		addCmpToGridBag(sleepTimeField,new GBC(1,2));
		
		
		pageEncodingLabel.setText(LanguageLoader.getString("RuleContentSetting.pageEncoding"));
		addCmpToGridBag(pageEncodingLabel,new GBC(2,2));

		pageEncodingField.setColumns(20);
		addCmpToGridBag(pageEncodingField,new GBC(3,2));
		
		depthLabel.setText(LanguageLoader.getString("Scan.settingDepth"));
		addCmpToGridBag(depthLabel,new GBC(0,3));

		depthField.setColumns(20);
		depthField.setInputVerifier(new IntegerVerifier(this, false, 1, 10000));
		addCmpToGridBag(depthField,new GBC(1,3));
		
		onlyScanSelfLabel.setText(LanguageLoader.getString("Scan.settingOnlyScanSelf"));
		addCmpToGridBag(onlyScanSelfLabel,new GBC(2,3));
		onlyScanSelfYesButton.setText(LanguageLoader.getString("Common.yes"));
		onlyScanSelfNoButton.setText(LanguageLoader.getString("Common.no"));
		onlyScanSelfYesButton.setSelected(true);
		onlyScanSelfValue = Constant.YES;
		onlyScanSelfYesButton.setBackground(null);
		onlyScanSelfNoButton.setBackground(null);
		onlyScanSelfButtonGroup.add(onlyScanSelfYesButton);
		onlyScanSelfButtonGroup.add(onlyScanSelfNoButton);
		JPanel onlyScanSelfPanel = createFlowLayoutJPanel(onlyScanSelfYesButton,onlyScanSelfNoButton);
		addCmpToGridBag(onlyScanSelfPanel,new GBC(3,3));
		
		proxyLabel.setText(LanguageLoader.getString("RuleContentSetting.proxy"));
		addCmpToGridBag(proxyLabel,new GBC(0,4));
		proxyYesButton.setText(LanguageLoader.getString("Common.yes"));
		proxyNoButton.setText(LanguageLoader.getString("Common.no"));
		proxyNoButton.setSelected(true);
		proxySelectValue = Constant.NO;
		proxyYesButton.setBackground(null);
		proxyNoButton.setBackground(null);
		proxyButtonGroup.add(proxyYesButton);
		proxyButtonGroup.add(proxyNoButton);
		JPanel proxyPanel = createFlowLayoutJPanel(proxyYesButton,proxyNoButton);
		addCmpToGridBag(proxyPanel,new GBC(1,4));
		
		
		
		openMonitorLabel.setText(LanguageLoader.getString("Scan.settingOpenMonitor"));
		addCmpToGridBag(openMonitorLabel,new GBC(2,4));
		openMonitorYesButton.setText(LanguageLoader.getString("Common.yes"));
		openMonitorNoButton.setText(LanguageLoader.getString("Common.no"));
		openMonitorNoButton.setSelected(true);
		openMonitorValue = Constant.NO;
		openMonitorYesButton.setBackground(null);
		openMonitorNoButton.setBackground(null);
		openMonitorButtonGroup.add(openMonitorYesButton);
		openMonitorButtonGroup.add(openMonitorNoButton);
		JPanel openMonitorPanel = createFlowLayoutJPanel(openMonitorYesButton,openMonitorNoButton);
		addCmpToGridBag(openMonitorPanel,new GBC(3,4));
		
		
		proxyAddressLabel.setText(LanguageLoader.getString("RuleContentSetting.proxyAddress"));
		addCmpToGridBag(proxyAddressLabel,new GBC(0,5));

		proxyAddressField.setColumns(20);
		addCmpToGridBag(proxyAddressField,new GBC(1,5));
		
		
		proxyPortLabel.setText(LanguageLoader.getString("RuleContentSetting.proxyPort"));
		addCmpToGridBag(proxyPortLabel,new GBC(2,5));

		proxyPortField.setColumns(20);
		proxyPortField.setInputVerifier(new IntegerVerifier(this, true, 1, 10000));
		addCmpToGridBag(proxyPortField,new GBC(3,5));
		
		addCmpToGridBag(new JLabel(),new GBC(0,6,4,1));
	}
	/**
	 * 填充页面控件数据
	 * <p>方法说明:</p>
	 * @auther DuanYong
	 * @since 2012-12-3 上午10:54:08
	 * @param ruleBaseBean
	 * @return void
	 */
	protected void fillComponentData(RuleBaseBean ruleBaseBean){
		logger.info("填充页面控件数据");
		if(StringUtils.isNotBlank(ruleBaseBean.getRuleName())){
			siteField.setText(ruleBaseBean.getRuleName());
		}
		if(StringUtils.isNotBlank(ruleBaseBean.getUrlRepairUrl())){
			linkField.setText(ruleBaseBean.getUrlRepairUrl());
		}
		pageEncodingField.setText(ruleBaseBean.getPageEncoding());
		sleepTimeField.setText(String.valueOf(ruleBaseBean.getPauseTime()));
		depthField.setText(String.valueOf(ruleBaseBean.getDepth()));
		
		if(Constant.NO.equals(ruleBaseBean.getReplaceLinkFlag())){
			onlyScanSelfNoButton.setSelected(true);
			onlyScanSelfValue = Constant.NO;
		}else{
			onlyScanSelfYesButton.setSelected(true);
			onlyScanSelfValue = Constant.YES;
		}
		
		if(Constant.YES.equals(ruleBaseBean.getGatherOrderFlag())){
			openMonitorYesButton.setSelected(true);
			openMonitorValue = Constant.YES;
		}else{
			openMonitorNoButton.setSelected(true);
			openMonitorValue = Constant.NO;
		}
	
		if(Constant.YES.equals(ruleBaseBean.getUseProxyFlag())){
			proxyYesButton.setSelected(true);
			proxySelectValue = Constant.YES;
			changeProxyState(true);
		}else{
			proxyNoButton.setSelected(true);
			proxySelectValue = Constant.NO;
			changeProxyState(false);
		}
		proxyAddressField.setText(ruleBaseBean.getProxyAddress());
		
		proxyPortField.setText(ruleBaseBean.getProxyPort());
		
		
	}
	
	/**
	 * 初始化监听事件
	 * <p>方法说明:</p>
	 * @auther DuanYong
	 * @since 2012-11-16 上午9:36:42
	 * @return void
	 */
	protected void initActionListener(){
		class OnlyScanSelfBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (onlyScanSelfYesButton.isSelected()) {
					 	onlyScanSelfValue = Constant.YES;
	                } else if (onlyScanSelfNoButton.isSelected()) {
	                	onlyScanSelfValue = Constant.NO;
	                }
			}
		}
		onlyScanSelfYesButton.addActionListener(new OnlyScanSelfBtnActionAdapter());
		onlyScanSelfNoButton.addActionListener(new OnlyScanSelfBtnActionAdapter());
		
		class OpenMonitorBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (openMonitorYesButton.isSelected()) {
					 	openMonitorValue = Constant.YES;
	                } else if (openMonitorNoButton.isSelected()) {
	                	openMonitorValue = Constant.NO;
	                }
			}
		}
		openMonitorYesButton.addActionListener(new OpenMonitorBtnActionAdapter());
		openMonitorNoButton.addActionListener(new OpenMonitorBtnActionAdapter());
		
		class ProxyBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (proxyYesButton.isSelected()) {
					 	proxySelectValue = Constant.YES;
					 	changeProxyState(true);
	                } else if (proxyNoButton.isSelected()) {
	                	proxySelectValue = Constant.NO;
	                	changeProxyState(false);
	                }
			}
		}
		proxyYesButton.addActionListener(new ProxyBtnActionAdapter());
		proxyNoButton.addActionListener(new ProxyBtnActionAdapter());
		
		
	}
	private void changeProxyState(boolean b){
		proxyAddressField.setEnabled(b);
		proxyAddressLabel.setEnabled(b);
		proxyPortField.setEnabled(b);
		proxyPortLabel.setEnabled(b);
	}
	
	
	
	
	@Override
	protected RuleBaseBean populateData() {
		RuleBaseBean ruleBaseBean = new RuleBaseBean();
		ruleBaseBean.setRuleName(siteField.getText());
		ruleBaseBean.setUrlRepairUrl(linkField.getText());
		ruleBaseBean.setDepth(Integer.valueOf(depthField.getText()));
		ruleBaseBean.setPauseTime(Integer.valueOf(sleepTimeField.getText()));
		ruleBaseBean.setPageEncoding(pageEncodingField.getText());
		ruleBaseBean.setReplaceLinkFlag(this.onlyScanSelfValue);
		ruleBaseBean.setUseProxyFlag(this.proxySelectValue);
		ruleBaseBean.setGatherOrderFlag(this.openMonitorValue);
		ruleBaseBean.setProxyAddress(proxyAddressField.getText());
		ruleBaseBean.setProxyPort(proxyPortField.getText());
		return ruleBaseBean;
	}
	
	public void initData(RuleBaseBean t){
		if(t == null){
			t = new RuleBaseBean();
		}
		fillComponentData(t);
	}
	/**
	 * 完成销毁动作
	 */
	public void dispose(){
		super.dispose();
	}
}

