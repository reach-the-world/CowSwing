/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.ui.view.panel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.loader.ImageLoader;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.core.utils.JsonUtils;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.javacoo.cowswing.plugin.gather.service.beans.CrawlerDiyDataConfigBean;
import org.javacoo.cowswing.plugin.gather.service.beans.SelectValueBean;
import org.javacoo.cowswing.plugin.gather.ui.model.CrawlerDiyDataMapFieldsTabelModel;
import org.javacoo.cowswing.ui.listener.TextVerifier;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;


/**
 * 复杂自定义数据设置
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2013-4-22 下午5:26:54
 * @version 1.0
 */
@Component("diyDataMapSettingPanel")
public class DiyDataMapSettingPanel extends AbstractContentPanel<CrawlerDiyDataConfigBean>{

	private static final long serialVersionUID = 1L;
	/**容器面板*/
	protected JComponent centerPane;
	/**自定义数据名称输入框*/
	private javax.swing.JTextField nameField;
	/**自定义数据名称标签*/
	private javax.swing.JLabel nameLabel;
	
	/**添加自定义数据Button*/
	private JButton addExtednsFieldsButton;
	/**删除自定义数据Button*/
	private JButton deleteExtednsFieldsButton;
	/**删除自定义数据JTable*/
	private JTable crawlerExtendFieldsTabel;
	/**删除自定义数据TabelMode*/
	private CrawlerDiyDataMapFieldsTabelModel dataModel;
	
	/**自定义数据值标签*/
	private javax.swing.JLabel valueLabel;
	/**自定义数据描述输入框*/
	private javax.swing.JTextField descField;
	/**自定义数据描述标签*/
	private javax.swing.JLabel descLabel;
	/**自定义数据帮助标签*/
	private javax.swing.JLabel helpLabel;
	/**自定义键值对形式值*/
	private String mapValue = "";
	
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#populateData()
	 */
	@Override
	protected CrawlerDiyDataConfigBean populateData() {
		if(checkData()){
			CrawlerDiyDataConfigBean crawlerDiyDataConfigBean = new CrawlerDiyDataConfigBean();
			crawlerDiyDataConfigBean.setName(nameField.getText());
			crawlerDiyDataConfigBean.setValue(getMapValue());
			crawlerDiyDataConfigBean.setDesc(descField.getText());
			crawlerDiyDataConfigBean.setType(GatherConstant.EXTEND_FIELDS_VALUE_TYPE_STATIC_KEY_MAP);
			return crawlerDiyDataConfigBean;
		}else{
			return null;
		}
	}
	protected void initActionListener(){
		
	}
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#initComponents()
	 */
	@Override
	protected void initComponents() {
		class AddExtednsFieldsAction extends AbstractAction{
			private static final long serialVersionUID = 1L;
			public AddExtednsFieldsAction(){
				super(LanguageLoader.getString("System.addDiyMap"),ImageLoader.getImageIcon("CrawlerResource.toolbarRuleAdd"));
			}
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				SelectValueBean selectValueBean = new SelectValueBean("","");
				dataModel.addRow(selectValueBean);
			}

		}
		class DeleteExtednsFieldsAction extends AbstractAction{
			private static final long serialVersionUID = 1L;
			public DeleteExtednsFieldsAction(){
				super(LanguageLoader.getString("System.deleteDiyMap"),ImageLoader.getImageIcon("CrawlerResource.toolbarRuleDelete"));
			}
			/* (non-Javadoc)
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				if(crawlerExtendFieldsTabel.getSelectedRow() != -1){
					for(Integer selectRow : crawlerExtendFieldsTabel.getSelectedRows()){
						dataModel.removeRow(selectRow);
					}
				}
				for(SelectValueBean selectValueBean: dataModel.getData()){
					logger.info(selectValueBean.toString());
				}
			}
		}
		

		this.setLayout(new BorderLayout());
		if(null == centerPane){
			centerPane = new JPanel();
			centerPane.setLayout(new GridBagLayout());
			
			nameLabel = new javax.swing.JLabel();
			nameField = new javax.swing.JTextField();
			
			
			
			valueLabel = new javax.swing.JLabel();
			
			descLabel = new javax.swing.JLabel();
			descField = new javax.swing.JTextField();
			
			helpLabel = new javax.swing.JLabel();
			
		
			
			
			nameLabel.setText(LanguageLoader.getString("System.Diy_name"));
			centerPane.add(nameLabel,new GBC(0,0).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));

			nameField.setColumns(20);
			nameField.setInputVerifier(new TextVerifier(this, false));
			centerPane.add(nameField,new GBC(1,0).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			valueLabel.setText(LanguageLoader.getString("System.Diy_value"));
			centerPane.add(valueLabel,new GBC(0,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			addExtednsFieldsButton = new JButton(new AddExtednsFieldsAction());
			deleteExtednsFieldsButton = new JButton(new DeleteExtednsFieldsAction());

			JPanel valuePanel = createFlowLayoutJPanel(addExtednsFieldsButton,deleteExtednsFieldsButton);
			centerPane.add(valuePanel,new GBC(1,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			JScrollPane js = new JScrollPane(getCrawlerExtendFieldsTable());
			centerPane.add(js,new GBC(0,2,2,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			
			
			descLabel.setText(LanguageLoader.getString("System.Diy_desc"));
			centerPane.add(descLabel,new GBC(0,5).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			descField.setColumns(20);
			centerPane.add(descField,new GBC(1,5).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			helpLabel.setText(LanguageLoader.getString("System.Diy_map_help"));
			centerPane.add(helpLabel,new GBC(0,6,2,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 0));
			
			centerPane.add(new JLabel(),new GBC(0,7,2,1).setAnchor(GridBagConstraints.SOUTHEAST)
					.setInsets(0,5,5,5)
					.setFill(GridBagConstraints.BOTH)
					.setWeight(0, 1));
			add(centerPane,BorderLayout.NORTH);
		}
		
		
		

	}
	/**
	 * <p>方法说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2013-3-17 下午9:08:40
	 * @return
	 * @return Component
	 */ 
	private java.awt.Component getCrawlerExtendFieldsTable() {
		if (crawlerExtendFieldsTabel == null) {
			crawlerExtendFieldsTabel = new JTable();
			dataModel = new CrawlerDiyDataMapFieldsTabelModel(getColumnNames());
			crawlerExtendFieldsTabel.setModel(dataModel);
			crawlerExtendFieldsTabel.setPreferredScrollableViewportSize(new Dimension(500, 70));
			crawlerExtendFieldsTabel.setFillsViewportHeight(true);
			//允许按列排序
			//crawlerExtendFieldsTabel.setAutoCreateRowSorter(true);
		}
		return crawlerExtendFieldsTabel;
	}
	private List<String> getColumnNames() {
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(LanguageLoader.getString("System.Diy_map_key"));
		columnNames.add(LanguageLoader.getString("System.Diy_map_value"));
		return columnNames;
	}
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#fillComponentData(java.lang.Object)
	 */
	@Override
	protected void fillComponentData(CrawlerDiyDataConfigBean t) {
		logger.info("填充页面控件数据");
		nameField.setText(t.getName());
		mapValue = t.getValue();
		descField.setText(t.getDesc());
		dataModel.setData(parseValueToList(t.getValue()));
	}
	
	private List<SelectValueBean> parseValueToList(String value){
		List<SelectValueBean> returnList = new ArrayList<SelectValueBean>();
		if(StringUtils.isNotBlank(value)){
			List tempList = (ArrayList)JsonUtils.formatStringToObject(value, ArrayList.class);
			for(Object obj : tempList){
				returnList.add((SelectValueBean)JsonUtils.formatStringToObject(obj.toString(), SelectValueBean.class));
			}
		}
		return returnList;
	}
	private String getMapValue(){
		if(CollectionUtils.isNotEmpty(dataModel.getData())){
			return JsonUtils.formatObjectToJsonString(dataModel.getData());
		}
		return mapValue;
	}
	private boolean checkData(){
		List<SelectValueBean> returnList = dataModel.getData();
		if(CollectionUtils.isEmpty(returnList)){
			JOptionPane.showMessageDialog(null,LanguageLoader.getString("System.Diy_map_value_isEmpty"),
					 LanguageLoader.getString("Common.alertTitle"),
					 JOptionPane.CLOSED_OPTION);
			return false;
		}
		for(SelectValueBean tempBean : returnList){
			if(StringUtils.isBlank(tempBean.getValue()) || StringUtils.isBlank(tempBean.getValueName())){
				JOptionPane.showMessageDialog(null,LanguageLoader.getString("System.Diy_map_value_isEmpty"),
						 LanguageLoader.getString("Common.alertTitle"),
						 JOptionPane.CLOSED_OPTION);
				return false;
			}
		}
		return true;
	}
	public void initData(CrawlerDiyDataConfigBean t){
		if(t == null){
			t = new CrawlerDiyDataConfigBean();
		}
		fillComponentData(t);
	}

}
