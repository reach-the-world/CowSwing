package org.javacoo.cowswing.plugin.gather.ui.view.panel.rule;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.service.beans.RuleCommentBean;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;
/**
 * 评论属性采集规则设置
 *@author DuanYong
 *@since 2012-11-10上午9:55:07
 *@version 1.0
 */
@Component("ruleCommentSettingPanel")
public class RuleCommentSettingPanel extends AbstractContentPanel<RuleCommentBean>{
	private static final long serialVersionUID = 1L;
	/**评论列表入口属性输入框*/
	private javax.swing.JTextArea commentListArea;
	/**评论列表入口属性标签*/
	private javax.swing.JLabel commentListLabel;
	/**评论列表入口过滤属性输入框*/
	private javax.swing.JTextArea commentListFilterArea;
	
	/**评论内容列表区域属性输入框*/
	private javax.swing.JTextArea commentContentListArea;
	/**评论内容列表区域属性标签*/
	private javax.swing.JLabel commentContentListLabel;
	/**评论内容列表区域过滤属性输入框*/
	private javax.swing.JTextArea commentContentListFilterArea;
	
	
	/**评论内容属性输入框*/
	private javax.swing.JTextArea commentContentArea;
	/**评论内容属性标签*/
	private javax.swing.JLabel commentContentLabel;
	/**评论内容过滤属性输入框*/
	private javax.swing.JTextArea commentContentFilterArea;
	
	
	/**评论链接区域属性输入框*/
	private javax.swing.JTextArea commentLinkArea;
	/**评论链接区域属性标签*/
	private javax.swing.JLabel commentLinkLabel;
	/**评论链接区域过滤属性输入框*/
	private javax.swing.JTextArea commentLinkFilterArea;
	

	/**
	 * 初始化面板控件
	 */
	protected void initComponents(){
		useGridBagLayout();
		// ====================页面组件初始化==================
					commentListLabel = new javax.swing.JLabel();
					commentListArea = new javax.swing.JTextArea(4,20);
					
					commentListFilterArea = new javax.swing.JTextArea(4,20);
					
					commentContentListLabel = new javax.swing.JLabel();
					commentContentListArea = new javax.swing.JTextArea(4,20);
					commentContentListFilterArea = new javax.swing.JTextArea(4,20);
					
					commentContentLabel = new javax.swing.JLabel();
					commentContentArea = new javax.swing.JTextArea(4,20);
					commentContentFilterArea = new javax.swing.JTextArea(4,20);
					
					commentLinkLabel = new javax.swing.JLabel();
					commentLinkArea = new javax.swing.JTextArea(4,20);
					commentLinkFilterArea = new javax.swing.JTextArea(4,20);
					// ====================页面组件设置==================
					JLabel contentLabel = new javax.swing.JLabel(LanguageLoader.getString("RuleContentSetting.content"));
					JLabel filterLabel = new javax.swing.JLabel(LanguageLoader.getString("RuleContentSetting.filter"));
					
					// 添加标签
					addCmpToGridBag(contentLabel,new GBC(1,0));
					
					addCmpToGridBag(filterLabel,new GBC(2,0));
					
					commentListLabel.setText(LanguageLoader.getString("RuleContentSetting.commentList"));
					addCmpToGridBag(commentListLabel,new GBC(0,1));

					JPanel commentListAreaPanel = new JPanel(new BorderLayout()); 
					commentListArea.setLineWrap(true);
					commentListArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentListAreaPanel.add(new JScrollPane(commentListArea));
					addCmpToGridBag(commentListAreaPanel,new GBC(1,1));
					
					JPanel commentListFilterAreaPanel = new JPanel(new BorderLayout()); 
					commentListFilterArea.setLineWrap(true);
					commentListFilterArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentListFilterAreaPanel.add(new JScrollPane(commentListFilterArea));
					addCmpToGridBag(commentListFilterAreaPanel,new GBC(2,1));
					
					
					
					commentContentListLabel.setText(LanguageLoader.getString("RuleContentSetting.commentContentList"));
					addCmpToGridBag(commentContentListLabel,new GBC(0,2));

					JPanel commentContentListAreaPanel = new JPanel(new BorderLayout()); 
					commentContentListArea.setLineWrap(true);
					commentContentListArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentContentListAreaPanel.add(new JScrollPane(commentContentListArea));
					addCmpToGridBag(commentContentListAreaPanel,new GBC(1,2));
					
					

					JPanel commentContentListFilterAreaPanel = new JPanel(new BorderLayout()); 
					commentContentListFilterArea.setLineWrap(true);
					commentContentListFilterArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentContentListFilterAreaPanel.add(new JScrollPane(commentContentListFilterArea));
					addCmpToGridBag(commentContentListFilterAreaPanel,new GBC(2,2));
					
					
					
					
					commentContentLabel.setText(LanguageLoader.getString("RuleContentSetting.commentContent"));
					addCmpToGridBag(commentContentLabel,new GBC(0,3));

					JPanel commentContentAreaPanel = new JPanel(new BorderLayout()); 
					commentContentArea.setLineWrap(true);
					commentContentArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentContentAreaPanel.add(new JScrollPane(commentContentArea));
					addCmpToGridBag(commentContentAreaPanel,new GBC(1,3));
					
					

					JPanel commentContentFilterAreaPanel = new JPanel(new BorderLayout()); 
					commentContentFilterArea.setLineWrap(true);
					commentContentFilterArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentContentFilterAreaPanel.add(new JScrollPane(commentContentFilterArea));
					addCmpToGridBag(commentContentFilterAreaPanel,new GBC(2,3));
					
					
					
					commentLinkLabel.setText(LanguageLoader.getString("RuleContentSetting.commentLink"));
					addCmpToGridBag(commentLinkLabel,new GBC(0,4));

					JPanel commentLinkAreaPanel = new JPanel(new BorderLayout()); 
					commentLinkArea.setLineWrap(true);
					commentLinkArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentLinkAreaPanel.add(new JScrollPane(commentLinkArea));
					addCmpToGridBag(commentLinkAreaPanel,new GBC(1,4));
					
					

					JPanel commentLinkFilterAreaPanel = new JPanel(new BorderLayout()); 
					commentLinkFilterArea.setLineWrap(true);
					commentLinkFilterArea.setWrapStyleWord(true);//激活断行不断字功能 
					commentLinkFilterAreaPanel.add(new JScrollPane(commentLinkFilterArea));
					addCmpToGridBag(commentLinkFilterAreaPanel,new GBC(2,4));
					
					addCmpToGridBag(new JLabel(),new GBC(0,3,3,1));
	}
	
	protected void fillComponentData(RuleCommentBean ruleCommentBean){
		commentListArea.setText(ruleCommentBean.getCommentIndexStart());
	
		commentListFilterArea.setText(ruleCommentBean.getCommentIndexEnd());
		
		commentContentListArea.setText(ruleCommentBean.getCommentAreaStart());
		
		commentContentListFilterArea.setText(ruleCommentBean.getCommentAreaEnd());
		
		commentContentArea.setText(ruleCommentBean.getCommentStart());
	
		commentContentFilterArea.setText(ruleCommentBean.getCommentEnd());
	
		commentLinkArea.setText(ruleCommentBean.getCommentLinkStart());
		
		commentLinkFilterArea.setText(ruleCommentBean.getCommentLinkEnd());
	}
	
	@Override
	protected RuleCommentBean populateData() {
		RuleCommentBean ruleCommentBean = new RuleCommentBean();
		ruleCommentBean.setCommentIndexStart(commentListArea.getText());
		ruleCommentBean.setCommentIndexEnd(commentListFilterArea.getText());
		ruleCommentBean.setCommentLinkStart(commentLinkArea.getText());
		ruleCommentBean.setCommentLinkEnd(commentLinkFilterArea.getText());
		ruleCommentBean.setCommentAreaStart(commentContentListArea.getText());
		ruleCommentBean.setCommentAreaEnd(commentContentFilterArea.getText());
		ruleCommentBean.setCommentStart(commentContentArea.getText());
		ruleCommentBean.setCommentEnd(commentContentFilterArea.getText());
		return ruleCommentBean;
	}
	public void initData(RuleCommentBean t){
		if(t == null){
			t = new RuleCommentBean();
		}
		fillComponentData(t);
	}

}
