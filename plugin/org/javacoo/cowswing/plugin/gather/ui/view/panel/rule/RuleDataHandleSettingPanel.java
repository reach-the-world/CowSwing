/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.ui.view.panel.rule;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;
import org.javacoo.cowswing.core.cache.ICowSwingCacheManager;
import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.loader.ImageLoader;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.plugin.gather.constant.GatherConstant;
import org.javacoo.cowswing.plugin.gather.service.beans.RuleDataHandleConfigBean;
import org.javacoo.cowswing.plugin.gather.service.beans.SelectValueBean;
import org.javacoo.cowswing.plugin.gather.ui.model.ExportTypeComboBoxModel;
import org.javacoo.cowswing.plugin.gather.ui.model.TemplateComboBoxModel;
import org.javacoo.cowswing.ui.view.panel.AbstractContentPanel;
import org.javacoo.cowswing.ui.view.panel.GBC;
import org.springframework.stereotype.Component;

/**
 * 数据处理设置
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-17上午10:29:58
 * @version 1.0
 */
@Component("ruleDataHandleSettingPanel")
public class RuleDataHandleSettingPanel extends AbstractContentPanel<RuleDataHandleConfigBean>{

	private static final long serialVersionUID = 1L;

	/**缓存管理*/
	@Resource(name="cowSwingCacheManager")
	private ICowSwingCacheManager crawlerCacheManager;
	/**是否使用FTP单选按钮*/
	private javax.swing.JRadioButton useFtpYesButton;
	/**是否使用FTP单选按钮*/
	private javax.swing.JRadioButton useFtpNoButton;
	/**是否使用FTP单选按钮组*/
	private javax.swing.ButtonGroup useFtpButtonGroup;
	/**是否使用FTP标签*/
	private javax.swing.JLabel useFtpLabel;
	
	/**导出集合单选按钮*/
	private javax.swing.JRadioButton exportListYesButton;
	/**导出集合单选按钮*/
	private javax.swing.JRadioButton exportListNoButton;
	/**导出集合单选按钮组*/
	private javax.swing.ButtonGroup exportListButtonGroup;
	/**导出集合标签*/
	private javax.swing.JLabel exportListLabel;
	
	/**数据导出标签*/
	private javax.swing.JLabel exportLabel;
	/**数据导出类型下拉*/
	private JComboBox exportTypeCombo;
	
	/**数据导出类型选择LIST*/
	private List<SelectValueBean> exportTypeList;
	
	/**数据导出保存目录输入框*/
	private javax.swing.JTextField savePathField;
	/**数据导出保存目录标签*/
	private javax.swing.JLabel savePathLabel;
	/**数据导出保存路径选择按钮*/
	private JButton savePathBtn;
	

	/**模板标签*/
	private javax.swing.JLabel templateLabel;
	/**模板下拉*/
	private JComboBox templateCombo;
	
	/**模板选择LIST*/
	private List<SelectValueBean> templateList;
	
	private String useExportSelectValue = Constant.NO;
	
	private String exportListSelectValue = Constant.NO;
	
	
	private Map<String,SelectValueBean> templateSelectMap;
	
	private Map<String,SelectValueBean> exportTypeSelectMap;

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#populateData()
	 */
	@Override
	protected RuleDataHandleConfigBean populateData() {
		RuleDataHandleConfigBean crawlerDataHandleConfigBean = new RuleDataHandleConfigBean();
		crawlerDataHandleConfigBean.setSavePath(savePathField.getText());
		if(null != templateCombo.getSelectedItem()){
			SelectValueBean tsv = (SelectValueBean)templateCombo.getSelectedItem();
			crawlerDataHandleConfigBean.setTemplateName(tsv.getValue());
		}
		
		crawlerDataHandleConfigBean.setUseExportFlag(useExportSelectValue);
		crawlerDataHandleConfigBean.setExportListFlag(exportListSelectValue);
		if(null != exportTypeCombo.getSelectedItem()){
			SelectValueBean esv = (SelectValueBean)exportTypeCombo.getSelectedItem();
			crawlerDataHandleConfigBean.setUseExportType(esv.getValue());
		}
		return crawlerDataHandleConfigBean;
	}
	protected void initActionListener(){
		class UseFtpBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (useFtpYesButton.isSelected()) {
					useExportSelectValue = Constant.YES;
					changeUseFtpInputState(true);
	             } else {
	                useExportSelectValue = Constant.NO;
	                changeUseFtpInputState(false);
	             }
			}
		}
		useFtpYesButton.addActionListener(new UseFtpBtnActionAdapter());
		useFtpNoButton.addActionListener(new UseFtpBtnActionAdapter());
		
		class ExportListBtnActionAdapter implements  ActionListener{
			@Override
			public void actionPerformed(ActionEvent e) {
				 if (exportListYesButton.isSelected()) {
					 exportListSelectValue = Constant.YES;
					 reloadTemplateList(true);
	             } else {
	            	 exportListSelectValue = Constant.NO;
	            	 reloadTemplateList(false);
	             }
			}
		}
		exportListYesButton.addActionListener(new ExportListBtnActionAdapter());
		exportListNoButton.addActionListener(new ExportListBtnActionAdapter());
		
		
		savePathBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser jfc = new JFileChooser();
				jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				jfc.setDialogTitle("Open class File");
				int result = jfc.showOpenDialog(null);
				if (result == 1) {
					return; // 撤销则返回
				} else {
					File f = jfc.getSelectedFile();// f为选择到的目录
					savePathField.setText(f.getAbsolutePath()+Constant.SYSTEM_SEPARATOR);
				}
			}
		});
	}
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#initComponents()
	 */
	@Override
	protected void initComponents() {
		useGridBagLayout();
		useFtpLabel = new javax.swing.JLabel();
		useFtpYesButton = new javax.swing.JRadioButton();
		useFtpNoButton = new javax.swing.JRadioButton();
		useFtpButtonGroup = new javax.swing.ButtonGroup();
		

		exportListLabel = new javax.swing.JLabel();
		exportListYesButton = new javax.swing.JRadioButton();
		exportListNoButton = new javax.swing.JRadioButton();
		exportListButtonGroup = new javax.swing.ButtonGroup();
		
		
		exportLabel = new javax.swing.JLabel();
	
		exportTypeCombo = new JComboBox(new ExportTypeComboBoxModel());
		
		
		savePathLabel = new javax.swing.JLabel();
		savePathField = new javax.swing.JTextField();
		
		templateLabel = new javax.swing.JLabel();
		
		templateCombo = new JComboBox(new TemplateComboBoxModel());
		
		
		
		useFtpLabel.setText(LanguageLoader.getString("RuleContentSetting.useExport"));
		addCmpToGridBag(useFtpLabel,new GBC(0,0));
		useFtpYesButton.setText(LanguageLoader.getString("Common.yes"));
		useFtpNoButton.setText(LanguageLoader.getString("Common.no"));
		useFtpNoButton.setSelected(true);
		useExportSelectValue = Constant.NO;
		useFtpYesButton.setBackground(null);
		useFtpNoButton.setBackground(null);
		useFtpButtonGroup.add(useFtpYesButton);
		useFtpButtonGroup.add(useFtpNoButton);
		JPanel useFtpPanel = createFlowLayoutJPanel(useFtpYesButton,useFtpNoButton);
		addCmpToGridBag(useFtpPanel,new GBC(1,0));
		
		
		exportListLabel.setText(LanguageLoader.getString("RuleContentSetting.exportList"));
		addCmpToGridBag(exportListLabel,new GBC(2,0));
		exportListLabel.setBounds(340, 15, 250, 15);
		exportListYesButton.setText(LanguageLoader.getString("Common.yes"));
		exportListNoButton.setText(LanguageLoader.getString("Common.no"));
		exportListNoButton.setSelected(true);
		exportListSelectValue = Constant.NO;
		exportListYesButton.setBackground(null);
		exportListNoButton.setBackground(null);
		exportListButtonGroup.add(exportListYesButton);
		exportListButtonGroup.add(exportListNoButton);
		JPanel exportListPanel = createFlowLayoutJPanel(exportListYesButton,exportListNoButton);
		addCmpToGridBag(exportListPanel,new GBC(3,0));
		
		
		
		
		
		
		exportLabel.setText(LanguageLoader.getString("RuleContentSetting.exportType"));
		addCmpToGridBag(exportLabel,new GBC(0,1));
		addCmpToGridBag(exportTypeCombo,new GBC(1,1,3,1));
		
		
		savePathLabel.setText(LanguageLoader.getString("RuleContentSetting.savePath"));
		addCmpToGridBag(savePathLabel,new GBC(0,2));
		
		savePathField.setColumns(20);
		savePathBtn = new JButton(LanguageLoader.getString("RuleContentSetting.savePathBtn"),ImageLoader.getImageIcon("CrawlerResource.toolImageBrowse"));
		JPanel savePathPanel = createFlowLayoutJPanel(savePathField,savePathBtn);
		addCmpToGridBag(savePathPanel,new GBC(1,2,3,1));
		
		
		templateLabel.setText(LanguageLoader.getString("RuleContentSetting.templateName"));
		addCmpToGridBag(templateLabel,new GBC(0,3));
		
		addCmpToGridBag(templateCombo,new GBC(1,3,3,1));
		
		
		
		exportTypeList = new ArrayList<SelectValueBean>();
		exportTypeList.add(new SelectValueBean(LanguageLoader.getString("RuleContentSetting.exportTypeTxt"),GatherConstant.EXPORT_TYPE_TXT));
		exportTypeList.add(new SelectValueBean(LanguageLoader.getString("RuleContentSetting.exportTypeWord"),GatherConstant.EXPORT_TYPE_WORD));
		exportTypeList.add(new SelectValueBean(LanguageLoader.getString("RuleContentSetting.exportTypeExcel"),GatherConstant.EXPORT_TYPE_EXCEL));
		exportTypeList.add(new SelectValueBean(LanguageLoader.getString("RuleContentSetting.exportTypePdf"),GatherConstant.EXPORT_TYPE_PDF));
		exportTypeList.add(new SelectValueBean(LanguageLoader.getString("RuleContentSetting.exportTypeHtml"),GatherConstant.EXPORT_TYPE_HTML));
		exportTypeSelectMap = new HashMap<String,SelectValueBean>();
		for(SelectValueBean sv : exportTypeList){
			exportTypeSelectMap.put(sv.getValue(), sv);
		}
		templateList = new ArrayList<SelectValueBean>();
		templateSelectMap = new HashMap<String,SelectValueBean>();
		reloadTemplateList(false);
		
		addCmpToGridBag(new JLabel(),new GBC(0,3,3,1));
	}
	public void initData(RuleDataHandleConfigBean t){
		if(null == t){
			t = new RuleDataHandleConfigBean();
		}
		fillComponentData(t);
	}
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.ui.view.panel.AbstractContentPanel#fillComponentData(java.lang.Object)
	 */
	@Override
	protected void fillComponentData(RuleDataHandleConfigBean t) {
		
		exportTypeCombo.setModel(new ExportTypeComboBoxModel(exportTypeList));
		exportTypeCombo.repaint();
		
		if(null != t){
			exportTypeCombo.setSelectedItem(exportTypeSelectMap.get(t.getUseExportType()));
		}else{
			exportTypeCombo.setSelectedItem("");
		}
		
		templateCombo.setModel(new ExportTypeComboBoxModel(templateList));
		templateCombo.repaint();
		
		if(StringUtils.isNotBlank(t.getTemplateName())){
			templateCombo.setSelectedItem(templateSelectMap.get(t.getTemplateName()));
		}else{
			templateCombo.setSelectedItem("");
		}
		
		if(Constant.YES.equals(t.getUseExportFlag())){
			useFtpYesButton.setSelected(true);
			useExportSelectValue = Constant.YES;
			changeUseFtpInputState(true);
		}else{
			useFtpNoButton.setSelected(true);
			useExportSelectValue = Constant.NO;
			changeUseFtpInputState(false);
		}
		if(Constant.YES.equals(t.getExportListFlag())){
			exportListYesButton.setSelected(true);
			exportListSelectValue = Constant.YES;
			reloadTemplateList(true);
		}else{
			exportListNoButton.setSelected(true);
			exportListSelectValue = Constant.NO;
			reloadTemplateList(false);
		}
		if(StringUtils.isNotBlank(t.getSavePath())){
			savePathField.setText(t.getSavePath());
		}else{
			savePathField.setText("");
		}
	}
	/**
	 * 改变FTP配置相关选项状态
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2013-4-24 上午11:10:29
	 * @version 1.0
	 * @exception 
	 * @param b
	 */
	private void changeUseFtpInputState(boolean b){
		exportLabel.setEnabled(b);
		exportTypeCombo.setEnabled(b);
		savePathField.setEnabled(b);
		savePathLabel.setEnabled(b);
		savePathBtn.setEnabled(b);
		templateLabel.setEnabled(b);
		templateCombo.setEnabled(b);
		exportListLabel.setEnabled(b);
		exportListYesButton.setEnabled(b);
		exportListNoButton.setEnabled(b);
	}
	/**
	 * 加载模板
	 * <p>说明:</p>
	 * <li></li>
	 * @auther DuanYong
	 * @since 2016年12月27日上午9:38:06
	 * @param b
	 */
	private void reloadTemplateList(boolean b){
		List<String> templateNameList = GatherConstant.SINGLE_TEMPLATE_NAME_LIST;
		if(b){
			templateNameList = GatherConstant.MULTIPLE_TEMPLATE_NAME_LIST;
		}
		templateList.clear();
		SelectValueBean svb = null;
		for(String s : templateNameList){
			svb = new SelectValueBean(s,s);
			templateList.add(svb);
			if(!templateSelectMap.containsKey(s)){
				templateSelectMap.put(s, svb);
			}
		}
		templateCombo.setModel(new ExportTypeComboBoxModel(templateList));
		templateCombo.repaint();
		templateCombo.updateUI();
	}

	/**
	 * @param crawlerCacheManager the crawlerCacheManager to set
	 */
	public void setCrawlerCacheManager(ICowSwingCacheManager crawlerCacheManager) {
		this.crawlerCacheManager = crawlerCacheManager;
	}
    
}
