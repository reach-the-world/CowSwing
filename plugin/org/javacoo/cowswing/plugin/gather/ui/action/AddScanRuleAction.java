package org.javacoo.cowswing.plugin.gather.ui.action;

import java.awt.event.ActionEvent;

import javax.annotation.Resource;
import javax.swing.AbstractAction;
import javax.swing.Action;

import org.javacoo.cowswing.core.constant.Constant;
import org.javacoo.cowswing.core.loader.ImageLoader;
import org.javacoo.cowswing.core.loader.LanguageLoader;
import org.javacoo.cowswing.main.CowSwingMainFrame;
import org.javacoo.cowswing.plugin.gather.ui.view.panel.ScanRuleListPage;
import org.springframework.stereotype.Component;


/**
 * 添加全站扫描规则
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2015-3-2 下午9:09:05
 * @version 1.0
 */
@Component("addScanRuleAction")
public class AddScanRuleAction extends AbstractAction{
	private static final long serialVersionUID = 1L;
	@Resource(name="cowSwingMainFrame")
	private CowSwingMainFrame crawlerMainFrame;
	/**
	 * 全站扫描列表页面
	 */
	@Resource(name="scanRuleListPage")
	private ScanRuleListPage ruleListPage;
	public AddScanRuleAction(){
		super(LanguageLoader.getString("Scan.add"),ImageLoader.getImageIcon("CrawlerResource.toolbarRuleAdd"));
		//快捷键
		putValue(Action.MNEMONIC_KEY, new Integer('L'));
		putValue(Action.SHORT_DESCRIPTION, LanguageLoader.getString("Scan.add"));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		ruleListPage.getRuleSettingDialog().init(crawlerMainFrame, Constant.OPTION_TYPE_ADD, LanguageLoader.getString("Scan.add"));
		ruleListPage.getRuleSettingDialog().setVisible(true);
	}

}
