/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.cowswing.plugin.gather.ui.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

import org.javacoo.cowswing.plugin.gather.service.beans.SelectValueBean;

/**
 * 数据导出类型下拉model
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * @author DuanYong
 * @since 2016-2-17下午2:52:38
 * @version 1.0
 */
public class ExportTypeComboBoxModel extends AbstractListModel implements ComboBoxModel{

	private static final long serialVersionUID = 1L;
    private SelectValueBean selectValueBean;
    private List<SelectValueBean> selectValueBeanList = new ArrayList<SelectValueBean>();
    public ExportTypeComboBoxModel(){
    }
    public ExportTypeComboBoxModel(List<SelectValueBean> selectValueBeanList){
    	this.selectValueBeanList.addAll(selectValueBeanList);
    }
	/* (non-Javadoc)
	 * @see javax.swing.ListModel#getElementAt(int)
	 */
	@Override
	public Object getElementAt(int index) {
		return this.selectValueBeanList.get(index);
	}

	/* (non-Javadoc)
	 * @see javax.swing.ListModel#getSize()
	 */
	@Override
	public int getSize() {
		return this.selectValueBeanList.size();
	}

	/* (non-Javadoc)
	 * @see javax.swing.ComboBoxModel#getSelectedItem()
	 */
	@Override
	public Object getSelectedItem() {
		// TODO Auto-generated method stub
		return this.selectValueBean;
	}

	/* (non-Javadoc)
	 * @see javax.swing.ComboBoxModel#setSelectedItem(java.lang.Object)
	 */
	@Override
	public void setSelectedItem(Object arg0) {
		this.selectValueBean = (SelectValueBean) arg0;
	}

}
