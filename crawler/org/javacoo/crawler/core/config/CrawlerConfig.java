package org.javacoo.crawler.core.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 爬虫参数配置
 * @author javacoo
 * @since 2012-02-29
 */
public abstract class CrawlerConfig {
	/**开启线程数*/
	public static int threadNum = Runtime.getRuntime().availableProcessors();
	/**一个任务的超时时间，单位为毫秒*/
	public static int taskTimeOut = 5000;
	/**访问失败时，允许重试访问的次数，默认0次*/
	public static int tryMaxNum = 0;
	/**httpclient 最大连接数,默认100*/
	public static int httpClientMaxConn = 100;
	/**httpclient route 可以理解为 运行环境机器 到 目标机器的一条线路，默认20*/
	public static int httpClientMaxRoute = 50;
	/**httpclient 连接超时，默认10秒*/
	public static int httpConnTimeout = 10000;
	/**httpclient 请求超时，默认10秒*/
	public static int httpSocketTimeout = 10000;
	/**资源保存根路径*/
	public static String resSaveRootPath = "/";
	/**资源保存相对路径,一般留空,默认按照年月生成文件夹，然后再按照当天时间生成子文件夹如：201202/29*/
	public static String resSavePath = "";
	/**允许采集进来的外部资源类型*/
	public static String extractResType = "doc,docx,excel,txt,pdf,rar";
	/**允许采集进来的外部媒体资源类型*/
	public static String extractMediaResType = "swf,flv";
	/**资源是否重命名,false:不重名,true:重命名,默认false*/
	public static String replaceResName = "false";
	/**代理服务器MAP列表,key为服务器地址,value为端口，配置格式为:address:port,address:port,.....*/
	public static List<Map<String,Integer>> proxyServerList = null;
	/**系统绝对路径*/
	public static String systemRootPath = System.getProperties().getProperty("user.dir")+System.getProperties().getProperty("file.separator");
	/**默认替换后字符*/
	public static String defaultWords = "";
	/**默认全局替换字符,格式:待替换字符1=替换后字符1,待替换字符2=替换后字符2,待替换字符3,待替换字符4...*/
	public static Map<String,String> defaultCommonReplaceWords = null;
	/**HTTP 状态代码*/
	public static Map<String, String> statusCodeMap = initHttpStatusCodeMap();
	/**入库状态码字符串,默认404,403*/
	public static String saveHttpStatusCode = "404,403";
	
	private static Map<String, String> initHttpStatusCodeMap() {
		Map<String, String> httpStatus = new HashMap<String, String>();
		httpStatus.put("001", "网络异常:请求超时");
		httpStatus.put("002", "URL格式错误：解析URL出错");
		httpStatus.put("003", "获取URL返回信息失败");
		
		httpStatus.put("100", "请求者应当继续提出请求");
		httpStatus.put("101", "请求者已要求服务器切换协议，服务器已确认并准备进行切换");
		
		httpStatus.put("200", "服务器已成功处理了请求");
		httpStatus.put("201", "请求成功且服务器已创建了新的资源");
		httpStatus.put("202", "服务器已接受了请求，但尚未对其进行处理");
		httpStatus.put("203", "服务器已成功处理了请求，但返回了可能来自另一来源的信息");
		httpStatus.put("204", "服务器成功处理了请求，但未返回任何内容");
		httpStatus.put("205", "服务器成功处理了请求，但未返回任何内容(重置内容)");
		httpStatus.put("206", "服务器成功处理了部分 GET 请求");
		
		httpStatus.put("300", "服务器根据请求可执行多种操作");
		httpStatus.put("301", "请求的网页已被永久移动到新位置");
		httpStatus.put("302", "服务器目前正从不同位置的网页响应请求，但请求者应继续使用原有位置来进行以后的请求");
		httpStatus.put("303", "当请求者应对不同的位置进行单独的 GET 请求以检索响应时，服务器会返回此代码");
		httpStatus.put("304", "自从上次请求后，请求的网页未被修改过");
		httpStatus.put("305", "请求者只能使用代理访问请求的网页");
		httpStatus.put("307", "服务器目前正从不同位置的网页响应请求，但请求者应继续使用原有位置来进行以后的请求");
		
		httpStatus.put("400", "服务器不理解请求的语法");
		httpStatus.put("401", "请求要求进行身份验证");
		httpStatus.put("403", "服务器拒绝请求");
		httpStatus.put("404", "服务器找不到请求的网页");
		httpStatus.put("405", "禁用请求中所指定的方法");
		httpStatus.put("406", "无法使用请求的内容特性来响应请求的网页");
		httpStatus.put("407", "请求者应当使用代理进行授权");
		httpStatus.put("408", "服务器等候请求时超时");
		httpStatus.put("409", "服务器在完成请求时发生冲突");
		httpStatus.put("410", "如果请求的资源已被永久删除，那么，服务器会返回此响应");
		httpStatus.put("411", "服务器不会接受包含无效内容长度标头字段的请");
		httpStatus.put("412", "服务器未满足请求者在请求中设置的其中一个前提条件");
		httpStatus.put("413", "服务器无法处理请求，因为请求实体过大，已超出服务器的处理能力");
		httpStatus.put("414", "请求的 URI（通常为网址）过长，服务器无法进行处理");
		httpStatus.put("415", "请求的格式不受请求页面的支持");
		httpStatus.put("416", "如果请求是针对网页的无效范围进行的，那么，服务器会返回此状态代码");
		httpStatus.put("417", "服务器未满足'期望'请求标头字段的要求");
		
		httpStatus.put("500", "服务器遇到错误，无法完成请求");
		httpStatus.put("501", "服务器不具备完成请求的功能");
		httpStatus.put("502", "服务器作为网关或代理，从上游服务器收到了无效的响应");
		httpStatus.put("503", "目前无法使用服务器（由于超载或进行停机维护）");
		httpStatus.put("504", "服务器作为网关或代理，未及时从上游服务器接收请求");
		httpStatus.put("505", "服务器不支持请求中所使用的 HTTP 协议版本");
		return httpStatus;  
	}
	
	
}
