package org.javacoo.crawler.core.processor.extractor;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.Url;
import org.javacoo.crawler.core.data.uri.CrawlLinkURI;



/**
 * 任务处理器接口-抽取URL内容实现类
 * @author javacoo
 * @since 2011-11-09
 */
public class ExtractorURIProcessor extends Extractor {
	static final String ABS_HTTP_URI_PATTERN = "^https?://[^\\s<>]*$";

	public ExtractorURIProcessor() {
		super();
	}
	@Override
	protected void extract(Task task) {
		if(task.getController().getCrawlScope().isGatherAll()){
			extractAll(task);
		}else{
			extractArea(task);
		}
	}
	/**
	 * 爬取局部所有URL
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-1 下午3:40:49
	 * @version 1.0
	 * @exception 
	 * @param task
	 */
	private void extractArea(Task task){
		//如果未设置内容采集参数
		if(task.getController().getCrawlScope().isOnlyGatherList()){
			return;
		}
		//List<CrawlLinkURI> crawlURIList = task.getController().getHtmlParserWrapper().getCrawlURIList(task.getContentBean().getOrginHtml(),task.getController().getCrawlScope().getSavePath(),task.getCrawlURI());
		List<CrawlLinkURI> crawlURIList = task.getController().getHtmlParserWrapper().getCrawlURIList(task);
		if(task.getController().getCrawlScope().isAllowRepeat()){
			//从采集历史表中检查是否已经采集过
			for(CrawlLinkURI crawlURI : crawlURIList){
				if(task.getController().getCrawlScope().getCrawlerPersistent().check(crawlURI.getUrl().getUrl(),task.getController().getCrawlScope().getId())){
					crawlURIList.remove(crawlURI);
				}
			}
		}
		task.getCrawlURI().setChildCrawlLinkURIList(crawlURIList);
	}
	/**
	 * 爬取当前所有URL
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-1 下午3:40:13
	 * @version 1.0
	 * @exception 
	 * @param task
	 */
	private void extractAll(Task task){
		//log.error("=======================当前任务URL:"+task.getCrawlURI().getUrl()+",深度："+task.getCrawlURI().getDepth()+",默认："+task.getController().getCrawlScope().getDepth()+",主机："+task.getCrawlURI().getHost());
		//只爬取网页URL,并且深度小于限制深度
		if(Constants.URL_TYPE_LINK.equals(task.getCrawlURI().getUrl().getType()) && task.getCrawlURI().getDepth() < task.getController().getCrawlScope().getDepth()){
			List<Url> urlList = null;
			//只监控主站相关链接
			if(task.getController().getCrawlScope().isOnlyDefaultHost()){
				//当前任务URL主机与默认主机相等
	        	if(task.getCrawlURI().getHost().contains(task.getController().getCrawlScope().getDefaultHostSuffix())){
	        		urlList = task.getController().getHtmlParserWrapper().getUrlList(task.getContentBean().getOrginHtml());
	        	}
			}else{//全部监控
				urlList = task.getController().getHtmlParserWrapper().getUrlList(task.getContentBean().getOrginHtml());
			}
			if(CollectionUtils.isNotEmpty(urlList)){
				for(Url url : urlList){
					//检查本地是否已经爬取过
//					if(task.getController().getUrlCache().checkUrlIsExist(url.getUrl(), task.getController().getCrawlScope().getId())){
//						urlList.remove(url);
//						continue;
//					}
					//检查服务端是否已经爬取过
    				if(task.getController().getCrawlScope().getCrawlerPersistent().check(url.getUrl(),task.getController().getCrawlScope().getId())){
    					urlList.remove(url);
    				}
    			}
				task.getCrawlURI().setChildUrlList(urlList);
			}
			log.info("=======================当前任务URL:"+task.getCrawlURI().getUrl()+",深度："+task.getCrawlURI().getDepth()+",主机："+task.getCrawlURI().getHost());
		}
		
	}

}
