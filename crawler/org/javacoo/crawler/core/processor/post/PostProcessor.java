package org.javacoo.crawler.core.processor.post;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.javacoo.crawler.core.constants.Constants;
import org.javacoo.crawler.core.data.Task;
import org.javacoo.crawler.core.data.Url;
import org.javacoo.crawler.core.data.uri.CrawlLinkURI;
import org.javacoo.crawler.core.processor.AbstractProcessor;

/**
 * 任务处理器接口-提交链实现类
 * <li>做和此 任务相关操作的最后处理</li>
 * @author javacoo
 * @since 2011-11-09
 */
public class PostProcessor extends AbstractProcessor{

	public PostProcessor() {
		super();
	}

	@Override
	protected void innerProcess(Task task) {
		if(task.getController().getCrawlScope().isGatherAll()){
			postAll(task);
		}else{
			postArea(task);
		}
		log.info("当前任务："+task.getCrawlURI().getTryNum()+"次");
	}
	/**
	 * 提交局部连接
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-1 下午3:56:51
	 * @version 1.0
	 * @exception 
	 * @param task
	 */
	private void postArea(Task task){
		List<CrawlLinkURI> crawlURIList = task.getCrawlURI().getChildCrawlLinkURIList();
		if(CollectionUtils.isNotEmpty(crawlURIList) && task.getCrawlURI().isSeed()){
			for(CrawlLinkURI crawlLinkURI : crawlURIList){
				crawlLinkURI.setDepth(task.getCrawlURI().getDepth() + 1);
				task.getController().getFrontier().addCrawlLinkURIToWait(crawlLinkURI);
			}
		}
	}
	/**
	 * 提交全部连接
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-3-1 下午3:56:29
	 * @version 1.0
	 * @exception 
	 * @param task
	 */
	private void postAll(Task task){
		//只爬取网页URL
		if(Constants.URL_TYPE_LINK.equals(task.getCrawlURI().getUrl().getType())){
			List<Url> linkUrlList = task.getCrawlURI().getChildUrlList();
			if(CollectionUtils.isNotEmpty(linkUrlList)){
				CrawlLinkURI spiderURI = null;
				for(Url url : linkUrlList){
					spiderURI = new CrawlLinkURI();
					spiderURI.setParentURI(task.getCrawlURI());
					spiderURI.setDepth(task.getCrawlURI().getDepth() + 1);
					spiderURI.setUrl(url);
					spiderURI.setTitle(url.getTitle());
					spiderURI.setType(url.getType());
					spiderURI.setPathType(url.getPathType());
					task.getController().getFrontier().addCrawlLinkURIToWait(spiderURI);
					//log.error("=======================添加任务URL:"+spiderURI.getUrl()+",深度："+spiderURI.getDepth()+",主机："+spiderURI.getHost());
				}
			}
		}
		
	}

}
