package org.javacoo.crawler.core.thread.swaparea;

/**
 * ThreadLoca数据交换区Holder
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年5月15日下午7:15:41
 */
public class ThreadLocalSwapAreaHolder implements SwapAreaHolder {
	
	/**声明ThreadLocal变量*/		
	private ThreadLocal<SwapArea> holder = new ThreadLocal<SwapArea>();
	
	/**
	 * 
	 * 获取与当前线程上下文绑定的数据交换区实例
	 * <p>
	 * 
	 * @return SwapArea 数据交换区实例
	 */
	@Override
	public SwapArea getCurrentSwapArea() {
		return holder.get();
	}

	/**
	 * 
	 * 释放与当前线程上下文绑定的数据交换区实例
	 * <p>
	 *
	 * @return SwapArea 数据交换区实例
	 */
    @Override
	public SwapArea removeCurrentSwapArea() {
		SwapArea swapArea = holder.get();
		
		holder.remove();
		
		return swapArea;
	}

	/**
	 * 
	 * 将数据交换区实例绑定到当前线程上下文
	 * <p>
	 * 
	 * @param swapArea 数据交换区实例
	 */
    @Override
	public void setCurrentSwapArea(SwapArea swapArea) {
		holder.set(swapArea);
	}

}
