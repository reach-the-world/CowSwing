package org.javacoo.crawler.core.thread.swaparea;

/**
 * 内部数据交换区Holder
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年5月15日下午7:14:05
 */
public interface SwapAreaHolder {

	/**
	 * 
	 * 将数据交换区实例绑定到当前线程上下文
	 * <p>
	 * 
	 * @param swapArea 数据交换区实例
	 */
	void setCurrentSwapArea(SwapArea swapArea);
	
	/**
	 * 
	 * 获取与当前线程上下文绑定的数据交换区实例
	 * <p>
	 * 
	 * @return SwapArea 数据交换区实例
	 */		
	SwapArea getCurrentSwapArea();
	
	/**
	 * 
	 * 释放与当前线程上下文绑定的数据交换区实例
	 * <p>
	 *
	 * @return SwapArea 数据交换区实例	 
	 */		
	SwapArea removeCurrentSwapArea();
	
}
