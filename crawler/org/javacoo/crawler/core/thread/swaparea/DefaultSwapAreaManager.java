package org.javacoo.crawler.core.thread.swaparea;

/**
 * 默认数据交换区管理器
 * <p>说明:</p>
 * <li></li>
 * @auther DuanYong
 * @since 2016年5月15日下午7:17:08
 */
public class DefaultSwapAreaManager implements SwapAreaManager {
	/**
	 * 数据交换区存储管理器变量
	 */
	private SwapAreaHolder swapAreaHolder = new ThreadLocalSwapAreaHolder();

	

	/**
	 * 
	 * 为当前请求构建并初始化一个新的数据交换区实例。
	 * <p>
	 * 
	 * @return SwapArea 创建的数据交换区实例
	 */
	@Override
	public SwapArea buildNewSwapArea() {
		SwapArea swapArea = new DefaultSwapArea();
		this.swapAreaHolder.setCurrentSwapArea(swapArea);
		return swapArea;
	}

	/**
	 * 
	 * 获取当前请求的数据交换区实例。
	 * <p>
	 * 
	 * @return SwapArea 当前请求绑定的数据交换区实例
	 */
    @Override
	public SwapArea getCurrentSwapArea() {
		return this.swapAreaHolder.getCurrentSwapArea();
	}

	/**
	 * 
	 * 释放当前请求的数据交换区实例。
	 * <p>
	 * 
	 * @return SwapArea 当前请求绑定的数据交换区实例
	 */
    @Override
	public SwapArea releaseCurrentSwapArea() {
		return this.swapAreaHolder.removeCurrentSwapArea();
	}

}
