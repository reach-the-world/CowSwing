/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.crawler.core.util.parser;

import java.util.List;

/**
 * 解析接口
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2015-2-15 下午3:38:22
 * @version 1.0
 */
public interface Parser {
	/**
	 * 获取单值
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-2-15 下午3:56:36
	 * @version 1.0
	 * @exception 
	 * @param html 原始HTML
	 * @param expression 表达式
	 * @return 
	 */
	String get(String html,String expression);
	
	/**
	 * 获取列表
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-2-15 下午3:57:25
	 * @version 1.0
	 * @exception 
	 * @param html 原始HTML
	 * @param expression 表达式
	 * @return 结果列表
	 */
	List<String> getList(String html,String expression);
}
