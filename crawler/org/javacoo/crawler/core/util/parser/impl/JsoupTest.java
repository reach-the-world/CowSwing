package org.javacoo.crawler.core.util.parser.impl;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class JsoupTest {
	public static void main(String[] args) throws IOException{
		Document doc = Jsoup.connect("http://www.javacoo.com").get();
		//System.out.println(doc.body().text());
		//System.out.println(doc.body().data());
		System.out.println(doc.body().ownText());
		//System.out.println(doc.body().tagName());
	}
}
