/**
 * 如石子一粒,仰高山之巍峨,但不自惭形秽.
 * 若小草一棵,慕白杨之伟岸,却不妄自菲薄.
 */
package org.javacoo.crawler.core.util.parser.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.javacoo.crawler.core.util.parser.Parser;

/**
 * 正则表达式实现类
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2015-2-15 下午4:23:30
 * @version 1.0
 */
public class RegexParserImpl implements Parser{
	private static Log log =  LogFactory.getLog(RegexParserImpl.class);
	private final Map<String, Pattern> patternMap = new ConcurrentHashMap<String, Pattern>();
	/* (non-Javadoc)
	 * @see org.javacoo.crawler.core.util.parser.Parser#get(java.lang.String, java.lang.String)
	 */
	@Override
	public String get(String html, String expression) {
		List<String> resultList = getList(html,expression);
		if(CollectionUtils.isNotEmpty(resultList)){
			return resultList.get(0);
		}
		return "";
	}

	/* (non-Javadoc)
	 * @see org.javacoo.crawler.core.util.parser.Parser#getList(java.lang.String, java.lang.String)
	 */
	@Override
	public List<String> getList(String html, String expression) {
		List<String> resultList = new ArrayList<String>();
		if(StringUtils.isBlank(expression)){
			return resultList;
		}
		Pattern regex = compile(expression);
		Matcher matcher = regex.matcher(html);
	    if(matcher.find()){
	    	log.error("groupCount="+matcher.groupCount());
	        for (int i = 0; i < matcher.groupCount() + 1; i++) {
	        	if(null != matcher.group(i)){
	        		log.error("expression="+expression+",result="+matcher.group(i));
	        		resultList.add(matcher.group(i));
	        	}
	        }
	    }
		return resultList;
	}
	/**
	 * 编译表达式
	 * <p>方法说明:</>
	 * <li></li>
	 * @author DuanYong
	 * @since 2015-2-15 下午4:52:51
	 * @version 1.0
	 * @exception 
	 * @param expression
	 * @return
	 */
	private Pattern compile(String expression){
		Pattern regex = patternMap.get(expression);
		if(null == regex){
			try {
	            regex = Pattern.compile(expression, Pattern.DOTALL | Pattern.CASE_INSENSITIVE);
	            patternMap.put(expression, regex);
			} catch (PatternSyntaxException e) {
	        	log.error("编译表达式失败");
	        	e.printStackTrace();
	        }
		}
		return regex;
	}

}
