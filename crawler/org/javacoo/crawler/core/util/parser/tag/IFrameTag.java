package org.javacoo.crawler.core.util.parser.tag;

import org.htmlparser.tags.CompositeTag;
/**
 * 自定义标签
 * <li>IFrame类型 </li>
 * @author javacoo
 * @since 2012-05-22
 */
public class IFrameTag extends CompositeTag {
	private static final long serialVersionUID = 1L;
	private static final String[] mIds = new String[] {"IFRAME"};
    private static final String SRC = "SRC";
    private static final String NAME = "NAME";
    @Override
	public String[] getIds() {
		return mIds;
	}
    @Override
	public String[] getEndTagEnders()     
	{     
	   return mIds;     
	}   
	
	public String getIFrameLocation(){
		String ret = super.getAttribute(SRC);   
	    if (null == ret){
		      ret = ""; 
	    }else if (null != super.getPage()) {   
	      ret = super.getPage().getAbsoluteURL(ret);   
	    }   
	    return ret;  
	}
	public void setIFrameLocation(String value){
		this.setAttribute(SRC, value);
	}
	public String getIFrameName()
    {
        return (getAttribute (NAME));
    }
}
