package org.javacoo.crawler.core.cache;


/**
 * URL缓存
 * <li>用于去重</li>
 * @author DuanYong 
 * @since 2015-1-21下午8:38:53
 * @version 1.0
 */
public interface UrlCache {
	
	/**
	 * 检查URL
	 * <li></li>
	 * @author DuanYong 
	 * @since 2015-1-21下午8:43:16
	 * @version 1.0
	 * @param url
	 * @param ruleId
	 * @return
	 */
	public boolean checkUrlIsExist(String url,Integer ruleId);
	/**
	 * 清除
	 * <li></li>
	 * @author DuanYong 
	 * @since 2015-1-21下午8:54:25
	 * @version 1.0
	 */
	public void clear();
	/**
	 * 根据规则ID清除
	 * <li></li>
	 * @author DuanYong 
	 * @since 2015-1-21下午9:14:15
	 * @version 1.0
	 * @param ruleId
	 */
	public void clearById(Integer ruleId);
}
