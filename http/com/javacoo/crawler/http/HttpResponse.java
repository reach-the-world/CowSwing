package com.javacoo.crawler.http;

import java.io.InputStream;

/**
 * 
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月8日下午4:51:28
 */
public class HttpResponse {
	/**
	 * 状态码
	 */
	private int statusCode = 200;
	/**
	 * 内容类型
	 */
	private String contentType;
	/**
	 * 内容类型
	 */
	private long contentLength;
	/**
	 * String内容
	 */
	private String content;
	/**
	 * 字符集
	 */
	private String charset;

	/**
	 * 最后修改日期
	 */
	private String lastModified;
	/**
	 * 内容Stream
	 */
	private InputStream contentStream;
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public InputStream getContentStream() {
		return contentStream;
	}
	public void setContentStream(InputStream contentStream) {
		this.contentStream = contentStream;
	}
	public long getContentLength() {
		return contentLength;
	}
	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
	public String getLastModified() {
		return lastModified;
	}
	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}
	

}
