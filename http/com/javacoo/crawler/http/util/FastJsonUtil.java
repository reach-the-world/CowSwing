package com.javacoo.crawler.http.util;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.JSONLibDataFormatSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * FastJson工具类
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * 
 * @author DuanYong
 * @since 2016年8月1日下午2:34:29
 */
public class FastJsonUtil {

	private static final SerializeConfig CONFIG;

	static {
		CONFIG = new SerializeConfig();
		// 使用和json-lib兼容的日期输出格式
		CONFIG.put(java.util.Date.class, new JSONLibDataFormatSerializer());
		// 使用和json-lib兼容的日期输出格式
		CONFIG.put(java.sql.Date.class, new JSONLibDataFormatSerializer());
	}

	private static final SerializerFeature[] FEATURES = {
			// 输出空置字段
			SerializerFeature.WriteMapNullValue, 
			// list字段如果为null，输出为[]，而不是null
			SerializerFeature.WriteNullListAsEmpty, 
			// 数值字段如果为null，输出为0，而不是null
			SerializerFeature.WriteNullNumberAsZero, 
			// Boolean字段如果为null，输出为false，而不是null
			SerializerFeature.WriteNullBooleanAsFalse, 
			// 字符类型字段如果为null，输出为""，而不是null
			SerializerFeature.WriteNullStringAsEmpty 
	};

	public static String toJSONString(Object object) {
		return JSON.toJSONString(object, CONFIG, FEATURES);
//		return JSON.toJSONString(object, CONFIG, new PropertyFilter() {
//			// 过滤空值
//			@Override
//			public boolean apply(Object source, String name, Object value) {
//				if (DataUtil.isEmpty(value)) {
//					return false;
//				}
//				return true;
//			}
//		});
	}

	public static String toJSONNoFeatures(Object object) {
		return JSON.toJSONString(object, CONFIG);
	}

	public static Object toBean(String text) {
		return JSON.parse(text);
	}

	public static <T> T toBean(String text, Class<T> clazz) {
		return JSON.parseObject(text, clazz);
	}

	public static <T> Optional<T> toOptionalBean(String text, Class<T> clazz) {
		return Optional.ofNullable(JSON.parseObject(text, clazz));
	}

	public static <T> Object[] toArray(String text) {
		return toArray(text, null);
	}

	public static <T> Object[] toArray(String text, Class<T> clazz) {
		return JSON.parseArray(text, clazz).toArray();
	}

	public static <T> List<T> toList(String text, Class<T> clazz) {
		return JSON.parseArray(text, clazz);
	}

	/**
	 * 将javabean转化为序列化的json字符串
	 * 
	 * @param keyvalue
	 * @return
	 */
	public static Object beanToJson(Object keyvalue) {
		String textJson = JSON.toJSONString(keyvalue);
		Object objectJson = JSON.parse(textJson);
		return objectJson;
	}

	/**
	 * 将string转化为序列化的json字符串
	 * 
	 * @param keyvalue
	 * @return
	 */
	public static Object textToJson(String text) {
		Object objectJson = JSON.parse(text);
		return objectJson;
	}

	/**
	 * json字符串转化为map
	 * 
	 * @param s
	 * @return
	 */
	public static Map stringToCollect(String s) {
		Map m = JSONObject.parseObject(s, Map.class);
		return m;
	}

	/**
	 * 将map转化为string
	 * 
	 * @param m
	 * @return
	 */
	public static String collectToString(Map m) {
		String s = JSONObject.toJSONString(m);
		return s;
	}

}
