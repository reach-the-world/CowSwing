package com.javacoo.crawler.http.okhttp.cookie;

import java.util.List;

import com.javacoo.crawler.http.okhttp.cookie.store.CookieStore;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

public class CookieJarImpl implements CookieJar {
	private CookieStore cookieStore;

	public CookieJarImpl(CookieStore cookieStore) {
		if (cookieStore == null){
			throw new IllegalArgumentException("cookieStore不能为空");
		}
		this.cookieStore = cookieStore;
	}

	@Override
	public synchronized void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
		cookieStore.add(url, cookies);
	}

	@Override
	public synchronized List<Cookie> loadForRequest(HttpUrl url) {
		return cookieStore.get(url);
	}

	public CookieStore getCookieStore() {
		return cookieStore;
	}
}
