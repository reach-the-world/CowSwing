package com.javacoo.crawler.http.okhttp.builder;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.javacoo.crawler.http.okhttp.request.GetRequest;
import com.javacoo.crawler.http.okhttp.request.RequestCall;

/**
 * get请求构建器
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午2:10:39
 */
public class GetBuilder extends OkHttpRequestBuilder<GetBuilder> implements HasParamsable {
	@Override
	public RequestCall build() {
		if (params != null) {
			url = appendParams(url, params);
		}

		return new GetRequest(url, tag, params, headers, id).build();
	}

	protected String appendParams(String url, Map<String, String> params) {
		if (url == null || params == null || params.isEmpty()) {
			return url;
		}
		StringBuilder builder = new StringBuilder(url);
		for (Entry<String,String> entry :  params.entrySet()) {
			builder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		return builder.toString();
	}

	@Override
	public GetBuilder params(Map<String, String> params) {
		this.params = params;
		return this;
	}

	@Override
	public GetBuilder addParams(String key, String val) {
		if (this.params == null) {
			params = new LinkedHashMap<>();
		}
		params.put(key, val);
		return this;
	}
}
