package com.javacoo.crawler.http.okhttp.builder;

import com.javacoo.crawler.http.okhttp.OkHttpUtils;
import com.javacoo.crawler.http.okhttp.request.OtherRequest;
import com.javacoo.crawler.http.okhttp.request.RequestCall;

/**
 * 请求头构建器
 * <p>
 * 说明:
 * </p>
 * <li></li>
 * 
 * @author DuanYong
 * @since 2018年2月7日下午2:19:35
 */
public class HeadBuilder extends GetBuilder {
	@Override
	public RequestCall build() {
		return new OtherRequest(null, null, OkHttpUtils.METHOD.HEAD, url, tag, params, headers, id).build();
	}
}
