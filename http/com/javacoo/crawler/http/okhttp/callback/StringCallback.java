package com.javacoo.crawler.http.okhttp.callback;

import java.io.IOException;

import okhttp3.Response;
/**
 * 文本类型回调
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月7日下午2:37:44
 */
public abstract  class StringCallback extends Callback<String> {
	@Override
	public String parseNetworkResponse(Response response, int id) throws IOException {
		return response.body().string();
	}
}
