package com.javacoo.crawler.http;
/**
 * Http参数配置
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年2月9日上午9:17:27
 */
public class HttpConfig {
	/**
	 * 是否使用代理
	 */
	private boolean useProxy = false;
	/**
	 * 代理地址
	 */
	private String proxyAddress;
	/**
	 * 代理端口
	 */
	private int proxyPort;
	/**
	 * 连接超时(单位:毫秒)
	 */
	private long connectTimeout = 10000L;
	/**
	 * 写入超时(单位:毫秒)
	 */
	private long writeTimeout = 10000L;
	/**
	 * 读取超时(单位:毫秒)
	 */
	private long readTimeout = 10000L;

	public boolean isUseProxy() {
		return useProxy;
	}

	public void setUseProxy(boolean useProxy) {
		this.useProxy = useProxy;
	}

	public String getProxyAddress() {
		return proxyAddress;
	}

	public void setProxyAddress(String proxyAddress) {
		this.proxyAddress = proxyAddress;
	}

	public int getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}

	public long getConnectTimeout() {
		return connectTimeout;
	}

	public void setConnectTimeout(long connectTimeout) {
		this.connectTimeout = connectTimeout;
	}

	public long getWriteTimeout() {
		return writeTimeout;
	}

	public void setWriteTimeout(long writeTimeout) {
		this.writeTimeout = writeTimeout;
	}

	public long getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(long readTimeout) {
		this.readTimeout = readTimeout;
	}
	
	

}
